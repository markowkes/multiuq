! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module visit_restart
  use parallel
  use variables
  use grid
  use basis
  use math
  use iso_c_binding
  
  implicit none
  include 'silo_f9x.inc'

  interface
     function mkdir(path,mode) bind(c,name="mkdir")
       use iso_c_binding
       integer(c_int) :: mkdir
       character(kind=c_char,len=1) :: path(*)
       integer(c_int16_t), value :: mode
     end function mkdir
  end interface
  
  ! Time info
  integer :: nout_time

  ! Write count
  integer :: visit_count
  integer :: visit_freq
  
  ! Output variables
  integer :: output_n,output_max
  character(len=str_medium), dimension(:),   allocatable :: output_types
  character(len=str_medium), dimension(:),   allocatable :: output_names
  character(len=str_short),  dimension(:,:), allocatable :: output_compNames
  
  ! Silo database
  integer :: silo_comm,silo_rank,Nproc_node
  integer, dimension(:), allocatable :: group_ids,proc_nums
  real(WP), dimension(:,:),   allocatable ::  spatial_extents, myspatial_extents
  real(WP), dimension(:,:),   allocatable :: Uspatial_extents,Umyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Vspatial_extents,Vmyspatial_extents
  real(WP), dimension(:,:,:,:), allocatable :: data_extents,   mydata_extents
  character(len=60), dimension(:), allocatable :: names
  integer, dimension(:), allocatable :: lnames,types
  
  ! Mesh 
  integer, parameter :: nGhostVisit=1
  integer :: nxout,nyout,nzout
  integer :: noverxmin_out,noverymin_out
  integer :: noverxmax_out,noverymax_out
  integer :: imin_out,jmin_out
  integer :: imax_out,jmax_out
  integer, dimension(2) :: lo_offset, hi_offset
  
  ! Buffers
  real(WP), dimension(:,:), allocatable :: out_buf
  
end module visit_restart

! ======================================== !
!  Dump Visit Silo files - Initialization  !
! ======================================== !
subroutine visit_restart_init
  use visit_restart
  use io
  implicit none
  integer :: i,iunit,nchar
  logical :: file_exists
  character(len=5)  :: tmpchar
  character(len=60) :: tmpchar2
  character(len=22) :: buffer
  real(WP) :: current_time
  real(WP),dimension(:),allocatable :: visit_times

  ! Read output frequency form inputs
  call read_input('Write Frequency',visit_freq)
  
  ! Check for multiUQ.visit file and create folder if needed
  if (rank.eq.root) then
     inquire(file='Visit_restart/multiUQ.visit',exist=file_exists)
     if (file_exists) then
        ! Get number of lines in file
        iunit = 32
        open(iunit,file="Visit_restart/multiUQ.visit",form="formatted",iostat=ierr,status='old')  
        nout_time=0
        do
           read(iunit,*,end=1)
           nout_time=nout_time+1
        end do
1       continue
        close(iunit)        
        ! Read the file and keep times less than current time
        allocate(visit_times(nout_time))
        iunit = 32
        open(iunit,file="Visit_restart/multiUQ.visit",form="formatted",iostat=ierr,status='old')  
        ! Get current time (formatted correctly)
        write(buffer,'(ES12.5)') time-dt*1e-10_WP
        read(buffer,*) current_time
        do i=1,nout_time
           ! Read file
           read(iunit,'(5A,60A)') tmpchar,tmpchar2
           ! Extract time from string
           nchar=len_trim(tmpchar2)
           read(tmpchar2(1:nchar-11),*) visit_times(i)
           ! Check if it is in the future and exit if true
           if (visit_times(i).ge.current_time) then
              nout_time=i-1
              exit
           end if
        end do
        close(iunit)
        ! Write new file with only past times
        iunit = 32
        open(iunit,file="Visit_restart/multiUQ.visit",form="formatted",iostat=ierr,status='replace')  
        do i=1,nout_time
           write(tmpchar2,'(ES12.5)') visit_times(i)
           tmpchar2='time_'//trim(adjustl(tmpchar2))//'/Visit.silo'
           write(iunit,'(A)') trim(adjustl(tmpchar2))
        end do
        deallocate(visit_times)
        close(iunit)
     else
        nout_time=0
        ! Create visit directory
        if (rank.eq.root) &
             call system('mkdir -p Visit_restart')
     end if
  end if

  ! Set visit counter
  visit_count=1
  
  ! Number of procs per file
  Nproc_node=16
  
  ! Create group_ids 
  allocate(group_ids(nproc))
  do i=1,nproc
     group_ids(i)=ceiling(real(i,WP)/real(Nproc_node,WP))
  end do
  ! Create proc_nums
  allocate(proc_nums(nproc))
  do i=1,nproc
     proc_nums(i)=mod(i-1,Nproc_node)+1
  end do
  
  ! Split procs into new communicators
  call MPI_COMM_SPLIT(COMM,group_ids(rank+1),MPI_UNDEFINED,silo_comm,ierr)
  call MPI_COMM_RANK(silo_comm,silo_rank,ierr)
  silo_rank=silo_rank+1
  
  ! Parse variables to output
  !call parser_getsize('Visit outputs',output_n)
  !allocate(output_names(output_n))
  !call parser_readchararray('Visit outputs',output_names(1:output_n))
  
  ! Allocate arrays
  allocate(out_buf(imino_:imaxo_,jmino_:jmaxo_))
  output_max=3*output_n
  allocate(output_types(output_max))
  allocate(output_compNames(3,output_n))
  allocate(  spatial_extents(4,nproc)); allocate( myspatial_extents(4,nproc))
  allocate(  data_extents(2,nproc,2,output_max)) 
  allocate(mydata_extents(2,nproc,2,output_max))
  allocate(names (nproc))
  allocate(lnames(nproc))
  allocate(types (nproc))
  
  ! Create output limits because Visit doesn't support external ghost cells
  noverxmin_out=nGhostVisit; noverxmax_out=nGhostVisit; imin_out=imin_-nGhostVisit; imax_out=imax_+nGhostVisit
  noverymin_out=nGhostVisit; noverymax_out=nGhostVisit; jmin_out=jmin_-nGhostVisit; jmax_out=jmax_+nGhostVisit
  if (rankx.eq.0   ) then; noverxmin_out=0; imin_out=imin_; end if
  if (ranky.eq.0   ) then; noverymin_out=0; jmin_out=jmin_; end if
  if (rankx.eq.px-1) then; noverxmax_out=0; imax_out=imax_; end if
  if (ranky.eq.py-1) then; noverymax_out=0; jmax_out=jmax_; end if
  nxout=imax_out-imin_out+1
  nyout=jmax_out-jmin_out+1
  hi_offset(1)=noverxmax_out
  hi_offset(2)=noverymax_out
  lo_offset(1)=noverxmin_out
  lo_offset(2)=noverymin_out
  
  return 
end subroutine visit_restart_init


! ===================================== !
!  Dump Visit Silo files - Write Files  !
! ===================================== !
subroutine visit_restart_data
  use visit_restart
  implicit none
  integer :: i,k,n,out
  integer :: dbfile,err,optlist,iunit
  character(len=str_medium) :: med_buffer
  character(len=13) :: dirname
  character(len=46) :: siloname
  character(len=30) :: folder
  character(len=11) :: buffer
  logical :: file_exists    
  
  ! Update output counter
  nout_time=nout_time+1

  ! Check if output this step
  visit_count=visit_count-1
  if (visit_count.gt.0) then
     return
  else 
     ! Reset counter
     visit_count=visit_freq
     ! Continue to write VisIt output
  end if

  ! Make foldername
  write(buffer,'(ES11.5)') time
  folder = 'Visit_restart/time_'//buffer

  if (rank.eq.root) then
     ! Create new directory
     call system('mkdir -p '// folder)
     !i = mkdir(folder, int(o'772',c_int16_t))
  end if
  call MPI_BARRIER(COMM,ierr)

  !  Create the group silo databases 
  ! =============================================

  ! Create the silo name for this processor
  write(siloname,'(A,I5.5,A)') trim(folder)//'/group',group_ids(rank+1),'.silo'

  ! Create the silo database
  if (silo_rank.eq.1) then
     err = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database created with multiUQ", 34, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')
     ierr = dbclose(dbfile)
  end if
  call MPI_BARRIER(silo_comm,ierr)
  
  !  Write the data
  ! =============================================
  do n=1,Nproc_node
     if (n.eq.silo_rank) then

        ! Open silo file 
        err = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)

        ! Create the silo directory
        write(dirname,'(I5.5)') proc_nums(rank+1)
        err = dbmkdir(dbfile,dirname,5,ierr)
        err = dbsetdir(dbfile,dirname,5)

        ! Write the variables
        mydata_extents=0.0_WP

        ! Write the data
        do k=1,ndof
           call write_var( 'u',k, u(:,:,k))
           call write_var( 'v',k, v(:,:,k))
           call write_var( 'G',k, G(:,:,k))
           call write_var('Pr',k,Pr(:,:,k))
        end do

        !  Write the meshes
        ! =============================================
        ! Write the regular mesh
        err = dbmkoptlist(2, optlist)
        err = dbaddiopt(optlist, DBOPT_HI_OFFSET, hi_offset)
        err = dbaddiopt(optlist, DBOPT_LO_OFFSET, lo_offset)
        err = dbputqm(dbfile,"Mesh",4,'xc',2,'yc',2,"zc",2, &
             real(x(imin_out:imax_out+1),WP),real(y(jmin_out:jmax_out+1),WP),DB_F77NULL, &
             (/nxout+1,nyout+1/),2,DB_DOUBLE,DB_COLLINEAR,optlist,ierr)
        err = dbfreeoptlist(optlist)

        ! Spatial extents (written in master silo's multimesh)
        myspatial_extents=0.0_WP
        myspatial_extents(1:2,rank+1)=(/x(imin_  ),y(jmin_  )/)
        myspatial_extents(3:4,rank+1)=(/x(imax_+1),y(jmax_+1)/)

        ! Close silo file
        ierr = dbclose(dbfile)
     end if
     call MPI_BARRIER(silo_comm,ierr)
  end do

  ! Communicate extents
  call MPI_ALLREDUCE( myspatial_extents, spatial_extents,4*nproc,mpi_double_precision,MPI_SUM,COMM,ierr)

  ! Create the master silo database (.visit file, multimesh, multivars)
  ! ===============================================================================
  if (rank.eq.root) then
     ! Append .visit file
     iunit = 32
     inquire(file="Visit_restart/multiUQ.visit",exist=file_exists)
     if (file_exists) then
        open(iunit,file="Visit_restart/multiUQ.visit",form="formatted",status="old",position="append",action="write")
     else
        open(iunit,file="Visit_restart/multiUQ.visit",form="formatted",status="new",action="write")
     end if
     write(iunit,'(A)') folder(15:)//'/Visit.silo'
     close(iunit)
     
     ! Set length of names
     err = dbset2dstrlen(len(names))
     
     ! Create the master silo database
     err = dbcreate(folder//'/Visit.silo', len_trim(folder//'/Visit.silo'), DB_CLOBBER, DB_LOCAL, &
          "Silo database created with multiUQ", 34, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')

     !  Write the multimeshes
     ! =======================
     do i=1,nproc
        write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/Mesh'
        lnames(i)=len_trim(names(i))
        types(i)=DB_QUAD_RECT
     end do
     err = dbmkoptlist(4,optlist)
     err = dbaddiopt(optlist, DBOPT_DTIME, time);
     err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 4)
     err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
     err = dbputmmesh(dbfile,  "Mesh", 4, nproc, names,lnames, types, optlist, ierr)
     err = dbfreeoptlist(optlist)

     !  Write the multivars
     ! =======================
     do k=1,ndof
        call write_mvar('u',k)
        call write_mvar('v',k)
        call write_mvar('G',k)
        call write_mvar('Pr',k)
     end do

     ! Close database
     ierr = dbclose(dbfile)
  end if
    
  return

contains
  subroutine write_var(name,dof,out_buf)
    implicit none
    character(len=*), intent(in) :: name
    integer, intent(in) :: dof
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: out_buf

    write(med_buffer,'(A,I0.3)') name,dof
    err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
         "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out), &
         (/nxout,nyout/), 2, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT,DB_F77NULL, ierr)
  end subroutine write_var

  subroutine write_mvar(name,dof)
    character(len=*), intent(in) :: name
    integer, intent(in) :: dof

    write(med_buffer,'(A,I0.3)') name,dof
    do i=1,nproc
       write(med_buffer,'(A,I0.3)') name,dof
       write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(med_buffer)
       lnames(i)=len_trim(names(i))
       types(i)=DB_QUADVAR
    end do
    err = dbputmvar(dbfile,trim(med_buffer),len_trim(med_buffer),nproc,names,lnames,types,DB_F77NULL,ierr)
  end subroutine write_mvar
    
end subroutine visit_restart_data
  
