! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module communication
  use variables
  use parallel
  use grid
  implicit none

contains
  !==========================!
  ! Communicate matrix along !
  ! edges of procs           !
  !==========================!
  subroutine comm_borders(A)
    use parallel
    implicit none

    real(WP), dimension(:,:,:), intent(inout) :: A
    real(WP), dimension(:,:,:), pointer :: bufferx_send, buffery_send
    real(WP), dimension(:,:,:), pointer :: bufferx_recv, buffery_recv
    integer :: sizex, sizey, sizez

    ! Allocate arrays
    sizex=size(A,1); sizey=size(A,2); sizez=size(A,3)
    allocate(bufferx_send(nghost,sizey,sizez))
    allocate(buffery_send(sizex,nghost,sizez))
    allocate(bufferx_recv(nghost,sizey,sizez))
    allocate(buffery_recv(sizex,nghost,sizez))

    ! ========== x direction =========== !

    ! Pass data to the left
    bufferx_send = A(1+nghost:nghost+nghost,:,:)
    bufferx_recv = A(nxo_-nghost+1:nxo_,:,:)
    call mpi_sendrecv(bufferx_send, sizey*nghost*ndof, mpi_double_precision, Lrecv, 1, &
         bufferx_recv, sizey*nghost*ndof, mpi_double_precision, Lsend, 1, comm_2D, status, ierr)
    A(nxo_-nghost+1:nxo_,:,:) = bufferx_recv

    ! Pass data to the right
    bufferx_send = A(nxo_-nghost-nghost+1:nxo_-nghost,:,:)
    bufferx_recv = A(1:nghost,:,:)
    call mpi_sendrecv(bufferx_send, sizey*nghost*ndof, mpi_double_precision, Rrecv, 2, &
         bufferx_recv, sizey*nghost*ndof, mpi_double_precision, Rsend, 2, comm_2D, status, ierr)
    A(1:nghost,:,:) = bufferx_recv

    ! ========== y direction =========== !

    ! Pass data to the bottom
    buffery_send = A(:,1+nghost:nghost+nghost,:)
    buffery_recv = A(:,nyo_-nghost+1:nyo_,:)
    call mpi_sendrecv(buffery_send, sizex*nghost*ndof, mpi_double_precision, Brecv, 3, &
         buffery_recv, sizex*nghost*ndof, mpi_double_precision, Bsend, 3, comm_2D, status, ierr)
    A(:,nyo_-nghost+1:nyo_,:) = buffery_recv

    ! Pass data to the top
    buffery_send = A(:,nyo_-nghost-nghost+1:nyo_-nghost,:)
    buffery_recv = A(:,1:nghost,:)
    call mpi_sendrecv(buffery_send, sizex*nghost*ndof, mpi_double_precision, Trecv, 4, &
         buffery_recv, sizex*nghost*ndof, mpi_double_precision, Tsend, 4, comm_2D, status, ierr)
    A(:,1:nghost,:) = buffery_recv

    deallocate(bufferx_send)
    deallocate(buffery_send)
    deallocate(bufferx_recv)
    deallocate(buffery_recv)

  end subroutine comm_borders

  !==========================!
  ! Communicate matrix along !
  ! edges of procs           !
  !==========================!
  subroutine comm_borders_2d(A)
    use parallel
    implicit none

    real(WP), dimension(:,:), intent(inout) :: A
    real(WP), dimension(:,:), pointer :: bufferx_send, buffery_send
    real(WP), dimension(:,:), pointer :: bufferx_recv, buffery_recv
    integer :: sizex, sizey

    ! Allocate arrays
    sizex=size(A,1); sizey=size(A,2)
    allocate(bufferx_send(nghost,sizey))
    allocate(buffery_send(sizex,nghost))
    allocate(bufferx_recv(nghost,sizey))
    allocate(buffery_recv(sizex,nghost))

    ! ========== x direction =========== !

    ! Pass data to the left
    bufferx_send=A(1+nghost:nghost+nghost,:)
    bufferx_recv=A(nxo_-nghost+1:nxo_,:)
    call mpi_sendrecv(bufferx_send, sizey*nghost, mpi_double_precision, Lrecv, 1, &
         bufferx_recv, sizey*nghost, mpi_double_precision, Lsend, 1, comm_2D, status, ierr)
    A(nxo_-nghost+1:nxo_,:)=bufferx_recv

    ! Pass data to the right
    bufferx_send=A(nxo_-nghost-nghost+1:nxo_-nghost,:)
    bufferx_recv=A(1:nghost,:)
    call mpi_sendrecv(bufferx_send, sizey*nghost, mpi_double_precision, Rrecv, 2, &
         bufferx_recv, sizey*nghost, mpi_double_precision, Rsend, 2, comm_2D, status, ierr)
    A(1:nghost,:)=bufferx_recv

    ! ========== y direction =========== !

    ! Pass data to the bottom
    buffery_send=A(:,1+nghost:nghost+nghost)
    buffery_recv=A(:,nyo_-nghost+1:nyo_)
    call mpi_sendrecv(buffery_send, sizex*nghost, mpi_double_precision, Brecv, 3, &
         buffery_recv, sizex*nghost, mpi_double_precision, Bsend, 3, comm_2D, status, ierr)
    A(:,nyo_-nghost+1:nyo_)=buffery_recv

    ! Pass data to the right
    buffery_send=A(:,nyo_-nghost-nghost+1:nyo_-nghost)
    buffery_recv=A(:,1:nghost)
    call mpi_sendrecv(buffery_send, sizex*nghost, mpi_double_precision, Trecv, 4, &
         buffery_recv, sizex*nghost, mpi_double_precision, Tsend, 4, comm_2D, status, ierr)
    A(:,1:nghost)=buffery_recv

    deallocate(bufferx_send)
    deallocate(buffery_send)
    deallocate(bufferx_recv)
    deallocate(buffery_recv)

    return
  end subroutine comm_borders_2d

  !==========================================!
  !          Pressure Communication          !
  !==========================================!
  subroutine P_comm_borders(A)
    use parallel
    implicit none

    real(WP), dimension(:,:,:), intent(inout) :: A
    real(WP), dimension(:,:), pointer :: bufferx_send, buffery_send
    real(WP), dimension(:,:), pointer :: bufferx_recv, buffery_recv
    integer :: sizex, sizey, sizez

    ! Allocate arrays
    sizex=size(A,1); sizey=size(A,2); sizez=size(A,3)
    allocate(bufferx_send(sizey,sizez))
    allocate(buffery_send(sizex,sizez))
    allocate(bufferx_recv(sizey,sizez))
    allocate(buffery_recv(sizex,sizez))

    ! ========== x direction =========== !

    ! Pass data to the left
    bufferx_send = A(nghost+1,:,:)
    bufferx_recv = A(nxo_-nghost+1,:,:)
    call mpi_sendrecv(bufferx_send, sizey*ndof, mpi_double_precision, Lrecv, 1, &
         bufferx_recv, sizey*ndof, mpi_double_precision, Lsend, 1, comm_2D, status, ierr)
    A(nxo_-nghost+1,:,:) = bufferx_recv

    ! Pass data to the right
    bufferx_send = A(nxo_-nghost,:,:)
    bufferx_recv = A(nghost,:,:)
    call mpi_sendrecv(bufferx_send, sizey*ndof, mpi_double_precision, Rrecv, 2, &
         bufferx_recv, sizey*ndof, mpi_double_precision, Rsend, 2, comm_2D, status, ierr)
    A(nghost,:,:) = bufferx_recv

    ! ========== y direction =========== !

    ! Pass data to the bottom
    buffery_send = A(:,nghost+1,:)
    buffery_recv = A(:,nyo_-nghost+1,:)
    call mpi_sendrecv(buffery_send, sizex*ndof, mpi_double_precision, Brecv, 3, &
         buffery_recv, sizex*ndof, mpi_double_precision, Bsend, 3, comm_2D, status, ierr)
    A(:,nyo_-nghost+1,:) = buffery_recv

    ! Pass data to the top
    buffery_send = A(:,nyo_-nghost,:)
    buffery_recv = A(:,nghost,:)
    call mpi_sendrecv(buffery_send, sizex*ndof, mpi_double_precision, Trecv, 4, &
         buffery_recv, sizex*ndof, mpi_double_precision, Tsend, 4, comm_2D, status, ierr)
    A(:,nghost,:) = buffery_recv

    deallocate(bufferx_send)
    deallocate(buffery_send)
    deallocate(bufferx_recv)
    deallocate(buffery_recv)

  end subroutine P_comm_borders

  !=============================================!
  !          2D Pressure Communication          !
  !=============================================!
  subroutine P_comm_borders_2d(A)
    use parallel
    implicit none

    real(WP), dimension(:,:), intent(inout) :: A
    real(WP), dimension(:), pointer :: bufferx_send, buffery_send
    real(WP), dimension(:), pointer :: bufferx_recv, buffery_recv
    integer :: sizex, sizey

    ! Allocate arrays
    sizex=size(A,1); sizey=size(A,2)
    allocate(bufferx_send(sizey)); allocate(bufferx_recv(sizey))
    allocate(buffery_send(sizex)); allocate(buffery_recv(sizex))

    ! ========== x direction =========== !

    ! Pass data to the left
    bufferx_send = A(1+nghost,:)
    bufferx_recv = A(nxo_-nghost+1,:)
    call mpi_sendrecv(bufferx_send, sizey, mpi_double_precision, Lrecv, 1, &
         bufferx_recv, sizey, mpi_double_precision, Lsend, 1, comm_2D, status, ierr)
    A(nxo_-nghost+1,:) = bufferx_recv

    ! Pass data to the right
    bufferx_send = A(nxo_-nghost,:)
    bufferx_recv = A(nghost,:)
    call mpi_sendrecv(bufferx_send, sizey, mpi_double_precision, Rrecv, 2, &
         bufferx_recv, sizey, mpi_double_precision, Rsend, 2, comm_2D, status, ierr)
    A(nghost,:) = bufferx_recv

    ! ========== y direction =========== !

    ! Pass data to the bottom
    buffery_send = A(:,1+nghost)
    buffery_recv = A(:,nyo_-nghost+1)
    call mpi_sendrecv(buffery_send, sizex, mpi_double_precision, Brecv, 3, &
         buffery_recv, sizex, mpi_double_precision, Bsend, 3, comm_2D, status, ierr)
    A(:,nyo_-nghost+1) = buffery_recv

    ! Pass data to the top
    buffery_send = A(:,nyo_-nghost)
    buffery_recv = A(:,nghost)
    call mpi_sendrecv(buffery_send, sizex, mpi_double_precision, Trecv, 4, &
         buffery_recv, sizex, mpi_double_precision, Tsend, 4, comm_2D, status, ierr)
    A(:,nghost) = buffery_recv

    deallocate(bufferx_send); deallocate(bufferx_recv)
    deallocate(buffery_send); deallocate(buffery_recv)

    return
  end subroutine P_comm_borders_2d

  !=========================!
  !     Sum real number     !
  !=========================!
  subroutine sum_real(A,B)
    implicit none
    real(WP), intent(in)  :: A
    real(WP), intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_double_precision,mpi_sum,comm_2D,ierr)

    return
  end subroutine sum_real

  !======================================!
  !          Max of real number          !
  !======================================!
  subroutine max_real(A,B)
    implicit none
    real(WP), intent(in)  :: A
    real(WP), intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_double_precision,mpi_max,comm_2D,ierr)

    return
  end subroutine max_real

  !======================================!
  !          Min of real number          !
  !======================================!
  subroutine min_real(A,B)
    implicit none
    real(WP), intent(in)  :: A
    real(WP), intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_double_precision,mpi_min,comm_2D,ierr)

    return
  end subroutine min_real

  !==================================!
  !          Sum of integer          !
  !==================================!
  subroutine sum_integer(A,B)
    implicit none
    integer, intent(in)  :: A
    integer, intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_integer,mpi_sum,comm_2D,ierr)

    return
  end subroutine sum_integer

  !===================================!
  !          Maximum integer          !
  !===================================!
  subroutine max_int(A,B)
    implicit none
    integer, intent(in)  :: A
    integer, intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_integer,mpi_max,comm_2D,ierr)

    return
  end subroutine max_int

  !===================================!
  !          Minimum integer          !
  !===================================!
  subroutine min_int(A,B)
    implicit none
    integer, intent(in)  :: A
    integer, intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_integer,mpi_min,comm_2D,ierr)

    return
  end subroutine min_int

end module communication


