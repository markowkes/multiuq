! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

! ==================================================== !
! Framework for automagically manipulating polynomials !
! ==================================================== !
module polynomial
  use variables
  implicit none

contains

  ! =============================================== !
  ! Compute nth derivative of func, store it in der !
  ! =============================================== !
  subroutine derivative_n(func,der,n)
    implicit none

    type(poly), intent(in)  :: func
    type(poly), intent(out) :: der
    integer,    intent(in)  :: n

    integer :: i

    ! Count number of terms
    der%n=0
    do i=1,func%n
       if (func%p(i).ge.n) der%n=der%n+1
    end do

    ! Allocate der to correct size
    if (associated(der%a)) deallocate(der%a)
    if (associated(der%p)) deallocate(der%p)
    allocate(der%a(der%n))
    allocate(der%p(der%n))

    ! Take nth derivative of func
    der%n=0
    do i=1,func%n
       if (func%p(i).ge.n) then
          der%n=der%n+1
          der%a(der%n)=func%a(i)*factorial_nm(func%p(i),func%p(i)-n+1)
          der%p(der%n)=func%p(i)-n
       end if
    end do

    return
  end subroutine derivative_n


  ! =============================================== !
  ! Compute nth primitive of func, store it in prim !
  ! =============================================== !
  subroutine primitive_n(func,prim,n)
    implicit none

    type(poly), intent(in)  :: func
    type(poly), intent(out) :: prim
    integer,    intent(in)  :: n

    integer :: i,k

    ! Allocate prim to correct size
    prim%n=func%n
    if (associated(prim%a)) deallocate(prim%a)
    if (associated(prim%p)) deallocate(prim%p)
    allocate(prim%a(prim%n))
    allocate(prim%p(prim%n))

    ! Initialize prim to func
    prim%a=func%a
    prim%p=func%p

    ! Taking nth primitive of func
    do k=1,n
       do i=1,prim%n
          prim%a(i)=prim%a(i)/real(prim%p(i)+1,QP)
          prim%p(i)=prim%p(i)+1
       end do
    end do

    return
  end subroutine primitive_n


  ! ========================================== !
  ! Compute integral of func between x1 and x2 !
  ! ========================================== !
  function integral(func,x1,x2)
    implicit none

    type(poly), intent(in) :: func
    real(QP),   intent(in) :: x1
    real(QP),   intent(in) :: x2
    real(QP) :: integral

    type(poly) :: prim

    ! Nullify
    nullify(prim%a)
    nullify(prim%p)

    ! Compute primitive of func
    call primitive_n(func,prim,1)

    ! Integrate
    integral = real(evalQ(prim,x2)-evalQ(prim,x1),QP)

    return
  end function integral

  !=========================================!
  !          Calculate a Factorial          !
  !=========================================!
  function factorial(nn)
    implicit none
    integer, intent(in) :: nn
    integer :: i
    real(QP) :: factorial

    ! Calculate the factorial
    factorial = 1.0_QP
    if (nn.gt.factorial) then
       do i = 1,nn
          factorial = factorial*real(i,QP)
       end do
    end if

  end function factorial

  !======================================================!
  !          Evaluate function at location zeta          !
  !======================================================!
  function eval(func,x)
    implicit none

    type(poly), intent(in) :: func
    real(WP),   intent(in) :: x
    real(QP) :: eval

    integer :: i

    ! Evaluate polynomial
    eval = 0.0_QP
    do i = 1,func%n
       eval = eval + func%a(i)*real(x,QP)**func%p(i)
    end do

    return
  end function eval
  
  !======================================================!
  !          Evaluate function at location zeta          !
  !======================================================!
  function evalQ(func,x)
    implicit none

    type(poly), intent(in) :: func
    real(QP),   intent(in) :: x
    real(QP) :: evalQ

    integer :: i

    ! Evaluate polynomial with quadruple precision
    evalQ = 0.0_QP
    do i = 1,func%n
       evalQ = evalQ + func%a(i)*real(x,QP)**func%p(i)
    end do

    return
  end function evalQ

  !======================================!
  !    Calculate a Definite Integral     !
  !======================================!
  function gauss_romberg(func,A,B)
    implicit none
    real(WP), intent(in) :: A,B
    type(poly), intent(in) :: func
    real(WP) :: gauss_romberg
    real(QP) :: best,best2
    integer :: i,n,m,p
    real(WP) :: aa,bb,h
    real(WP), dimension(3) :: x

    ! Romberg Integration for Gauss Tensor
    ! Uses Simpson's Rule as the basis
    ! Integrate until convergence is reached
    ri = 0.0_WP
    best = 1.0_WP; best2 = 0.0_WP
    n = 1
    do while (abs(best-best2).gt.0.000000000000000000001_WP)
       p = 2**(n-1)
       do i = 1,p
          aa = A + real((i-1),WP)*(B-A)/real(p,WP)
          bb = aa + (B-A)/real(p,WP)
          x = [aa,0.5_WP*(aa+bb),bb]
          h = 0.5_WP*(bb-aa)
          ri(n,1) = ri(n,1) + (h/3.0_WP)*(gaussf(x(1))*eval(func,x(1)) + &
               4.0_WP*gaussf(x(2))*eval(func,x(2)) + gaussf(x(3))*eval(func,x(3)))
       end do
       ! Iterate using Richardson Extrapolation
       if (n.gt.2) then
          do m = 2,n
             do i = 1,(n-(m-1))
                ri(i,m) = (1.0_WP/((4.0_WP**real(m,WP))-1.0_WP))*((4.0_WP**real(m,WP))*ri(i+1,m-1)-ri(i,m-1))
             end do
          end do
          best = ri(1,n); best2 = ri(2,n-1)
       end if
       n = n+1
    end do

    gauss_romberg = real(best,WP)

  end function gauss_romberg

  !========================================!
  !    Caculcate a value on a Gaussian     !
  !========================================!
  function gaussf(x)
    implicit none
    real(WP), intent(in) :: x
    real(WP) :: gaussf
    real(WP) :: c,zetamin,zetamax

    ! Constant A is one, can be multiplied in later
    ! Calculate the value based on [min,max]
    zetamin = -1.0_WP
    zetamax = +1.0_WP
    c = 1.0_WP/(2.0_WP*abs(zetamax-zetamin))
    ! To make sure expected value of levelset is 1
    ! c = 10.0_WP

    ! Answer
    gaussf = exp(-(x**2.0_WP)/(2.0_WP*c**2.0_WP))

  end function gaussf

  ! ======================== !
  ! Factorial: n*(n-1)*...*m !
  ! Note: n.ge.m is expected !
  ! ======================== !
  function factorial_nm(n,m)  
    implicit none

    integer, intent(in) :: n
    integer, intent(in) :: m
    real(WP) :: factorial_nm

    integer :: i

    ! Evaluate factorial
    factorial_nm = 1.0_WP
    do i = m,n
       factorial_nm = factorial_nm*real(i,WP)
    end do

  end function factorial_nm


  ! ========================================== !
  ! Binomial coefficient: C(n,k)=n!/(k!(n-k)!) !
  ! ========================================== !
  function binomial(n,k)
    implicit none

    integer, intent(in) :: n
    integer, intent(in) :: k
    real(WP) :: binomial

    ! Evaluate binomial coefficient
    binomial=factorial_nm(n,1)/(factorial_nm(k,1)*factorial_nm(n-k,1))

  end function binomial

end module polynomial



