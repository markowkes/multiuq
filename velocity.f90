! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module velocity
  use grid
  use variables
  use basis
  use communication
  implicit none

  ! Source term
  real(WP) :: mdot, mdot_init, mdot_local
  real(WP) :: Vconverge

end module velocity

!===================================!
!          Initialization           !
!===================================!
subroutine velocity_init
  use velocity
  use io
  implicit none

  ! Allocate arrays
  allocate(u      (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(v      (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(ustar  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(vstar  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(u_old  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(v_old  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhsU   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhsV   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(SC     (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(sourceU(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(sourceV(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(u_now  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(v_now  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Uint   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Vint   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(u_bot  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(v_lef  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! Derivatives
  allocate(dudx  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dvdx  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dudy  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dvdy  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dudxdx(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dudydy(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dvdxdx(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dvdydy(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! RHS
  allocate(Ucor(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Vcor(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! Divergence
  allocate(div_ux(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(div_vy(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(div_u (imino_:imaxo_,jmino_:jmaxo_,1:ndof))

  ! Fast Pressure Testing
  allocate(u_diff(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(v_diff(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  u_diff = 0.0_WP; v_diff = 0.0_WP
  
  ! Read boundary conditions
  call read_input('BC-y',bc_y)

  ! Set Convergence
  Vconverge = 0.00000000000001_WP

  ! Initialize arrays
  u     = 0.0_WP; v     = 0.0_WP; Ucor  = 0.0_WP; Vcor  = 0.0_WP
  ustar = 0.0_WP; vstar = 0.0_WP; u_old = 0.0_WP; v_old = 0.0_WP
  rhsU  = 0.0_WP; rhsV  = 0.0_WP; u_now = 0.0_WP; v_now = 0.0_WP
  sourceU = 0.0_WP; sourceV = 0.0_WP

  ! Find average divergence of simulation
  div_avg = 0.0_WP

  return
end subroutine velocity_init

!================================================!
!          Calculate Corrected Velocity          !
!================================================!
subroutine velocity_correct
  use velocity
  use variables
  implicit none

  integer :: kk,ll,tt,i
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: dPhdx, dPhdy

  ! Initialize arrays
  dPdx = 0.0_WP; dPdy = 0.0_WP; dPhdx = 0.0_WP; dPhdy = 0.0_WP
  Ucor = 0.0_WP; Vcor = 0.0_WP

  ! Gradient of pressure at cell faces
  dPdx(imin_:imax_+1,:,:) = (Pr(imin_:imax_+1,:,:)-Pr(imin_-1:imax_,:,:))*dxi
  dPdy(:,jmin_:jmax_+1,:) = (Pr(:,jmin_:jmax_+1,:)-Pr(:,jmin_-1:jmax_,:))*dyi
  call comm_borders(dPdx); call comm_borders(dPdy)

  if (.not.testPr) then
     ! Calculate gradient of pressure approximation
     dPhdx(imin_:imax_+1,:,:) = (P_hat(imin_:imax_+1,:,:)-P_hat(imin_-1:imax_,:,:))*dxi
     dPhdy(:,jmin_:jmax_+1,:) = (P_hat(:,jmin_:jmax_+1,:)-P_hat(:,jmin_-1:jmax_,:))*dyi
     call comm_borders(dPhdx); call comm_borders(dPhdy)

     ! Calculate correction for fast pressure solver
     do i = 1,Mn
        kk = Mlsave(i,1); ll = Mlsave(i,2); tt = Mlsave(i,3)
        Ucor(:,:,tt) = Ucor(:,:,tt) + iD_lef(:,:,kk)*dPhdx(:,:,ll)*M(kk,ll,tt)
        Vcor(:,:,tt) = Vcor(:,:,tt) + iD_bot(:,:,kk)*dPhdy(:,:,ll)*M(kk,ll,tt)
     end do
     Ucor = Ucor + (1.0_WP/rho_0)*(dPdx - dPhdx)
     Vcor = Vcor + (1.0_WP/rho_0)*(dPdy - dPhdy)
     call comm_borders(Ucor); call comm_borders(Vcor)
     
  else
     ! Calculate correction for test solver
     do i = 1,Mn
        kk = Mlsave(i,1); ll = Mlsave(i,2); tt = Mlsave(i,3)
        Ucor(:,:,tt) = Ucor(:,:,tt) + iD_lef(:,:,kk)*dPdx(:,:,ll)*M(kk,ll,tt)
        Vcor(:,:,tt) = Vcor(:,:,tt) + iD_bot(:,:,kk)*dPdy(:,:,ll)*M(kk,ll,tt)
     end do
     call comm_borders(Ucor); call comm_borders(Vcor)
     
  end if

  ! Update velocity
  u = ustar - dt*Ucor
  v = vstar - dt*Vcor
     
  ! Communicate
  call comm_borders(u); call comm_borders(v)

  return
end subroutine velocity_correct

!================================!
!          Velocity RHS          !
!================================!
subroutine velocity_rhs
  use velocity
  use io
  implicit none

  integer :: kk,ll,tt,i

  ! Zero arrays
  rhsU   = 0.0_WP; rhsV   = 0.0_WP
  dudx   = 0.0_WP; dudy   = 0.0_WP;  dvdx   = 0.0_WP; dvdy   = 0.0_WP
  dudxdx = 0.0_WP; dudydy = 0.0_WP;  u_bot  = 0.0_WP; v_lef  = 0.0_WP
  dvdxdx = 0.0_WP; dvdydy = 0.0_WP;  nu_bot = 0.0_WP; nu_lef = 0.0_WP

  ! Calculate derivatives
  ! Second derivatives
  dudxdx(imin_:imax_+1,:,:) = (u(imin_+1:imax_+2,:,:)-2.0_WP*u(imin_:imax_+1,:,:)+u(imin_-1:imax_,:,:))*(dxi**2)
  dudydy(:,jmin_:jmax_,:) = (u(:,jmin_+1:jmax_+1,:)-2.0_WP*u(:,jmin_:jmax_,:)+u(:,jmin_-1:jmax_-1,:))*(dyi**2)
  dvdxdx(imin_:imax_,:,:) = (v(imin_+1:imax_+1,:,:)-2.0_WP*v(imin_:imax_,:,:)+v(imin_-1:imax_-1,:,:))*(dxi**2)
  dvdydy(:,jmin_:jmax_+1,:) = (v(:,jmin_+1:jmax_+2,:)-2.0_WP*v(:,jmin_:jmax_+1,:)+v(:,jmin_-1:jmax_,:))*(dyi**2)
  call comm_borders(dudxdx); call comm_borders(dudydy)
  call comm_borders(dvdxdx); call comm_borders(dvdydy)

  ! First Derivatives
  dudx(imin_:imax_+1,:,:) = 0.5_WP*(u(imin_+1:imax_+2,:,:)-u(imin_-1:imax_,:,:))*dxi
  dudy(:,jmin_:jmax_,:) = 0.5_WP*(u(:,jmin_+1:jmax_+1,:)-u(:,jmin_-1:jmax_-1,:))*dyi
  dvdx(imin_:imax_,:,:) = 0.5_WP*(v(imin_+1:imax_+1,:,:)-v(imin_-1:imax_-1,:,:))*dxi
  dvdy(:,jmin_:jmax_+1,:) = 0.5_WP*(v(:,jmin_+1:jmax_+2,:)-v(:,jmin_-1:jmax_,:))*dyi
  call comm_borders(dudx); call comm_borders(dudy)
  call comm_borders(dvdx); call comm_borders(dvdy)

  ! Interpolate to locations needed
  u_bot(imin_:imax_,jmin_:jmax_+1,:) = 0.25_WP*(u(imin_:imax_,jmin_:jmax_+1,:) + &
       u(imin_+1:imax_+1,jmin_:jmax_+1,:) + u(imin_:imax_,jmin_-1:jmax_,:) + u(imin_+1:imax_+1,jmin_-1:jmax_,:))
  v_lef(imin_:imax_+1,jmin_:jmax_,:) = 0.25_WP*(v(imin_:imax_+1,jmin_:jmax_,:) + &
       v(imin_:imax_+1,jmin_+1:jmax_+1,:) + v(imin_-1:imax_,jmin_:jmax_,:) + v(imin_-1:imax_,jmin_+1:jmax_+1,:))
  call comm_borders(u_bot); call comm_borders(v_lef)

  ! Calculate the right hand side
  do i = 1,Mn
     kk = Mlsave(i,1); ll = Mlsave(i,2); tt = Mlsave(i,3)
     rhsU(:,:,tt) = rhsU(:,:,tt) - &
          (u(:,:,kk)*dudx(:,:,ll) + v_lef(:,:,kk)*dudy(:,:,ll))*M(kk,ll,tt) + &
          (nu_lef(:,:,kk)*dudxdx(:,:,ll) + nu_lef(:,:,kk)*dudydy(:,:,ll))*M(kk,ll,tt)
     rhsV(:,:,tt) = rhsV(:,:,tt) - &
          (u_bot(:,:,kk)*dvdx(:,:,ll) + v(:,:,kk)*dvdy(:,:,ll))*M(kk,ll,tt) + &
          (nu_bot(:,:,kk)*dvdxdx(:,:,ll) + nu_bot(:,:,kk)*dvdydy(:,:,ll))*M(kk,ll,tt)
  end do
  call comm_borders(rhsU); call comm_borders(rhsV)

  return
end subroutine velocity_rhs

!==============================!
!     Velocity source term     !
!==============================!
subroutine velocity_source
  use velocity
  implicit none

  integer :: i,j
  integer :: kk,ll,tt

  ! Initialize arrays
  sourceU = 0.0_WP; sourceV = 0.0_WP

  ! Force Mass Flow Rate
  if (isxper) then ! ... calculate source term

     ! Calculate current mass flow rate
     mdot_local = 0.0_WP
     do j = jmin_,jmax_
        do i = imin_,imax_
           mdot_local = mdot_local + 0.5_WP*(u(i,j,1)+u(i+1,j,1))
        end do
     end do
     call sum_real(mdot_local,mdot)

     ! Build source term based on difference 
     ! between initial and current   
     sourceU(:,:,1) = (mdot_init-mdot)/real((nx*ny),WP)
  end if

  if (use_multiphase) then

     ! Surface Tension Force
     do tt = 1,ndof
        do ll = 1,ndof
           do kk = 1,ndof
              if (M(kk,ll,tt).gt.epsilon(0.0_WP)) then
                 sourceU(:,:,tt) = sourceU(:,:,tt) + Kx(:,:,kk)*iD_lef(:,:,ll)*M(kk,ll,tt)
                 sourceV(:,:,tt) = sourceV(:,:,tt) + Ky(:,:,kk)*iD_bot(:,:,ll)*M(kk,ll,tt)
              end if
           end do
        end do
        ! Gravity
        if (gravity) sourceV(:,:,tt) = sourceV(:,:,tt) - Gr(tt)
     end do

  end if

  ! Communicate
  call comm_borders(sourceU)
  call comm_borders(sourceV)

  return
end subroutine velocity_source

!==================================================!
!          Velocity Source Initialization          !
!==================================================!
subroutine velocity_source_init
  use velocity
  implicit none

  integer :: i,j

  ! Calculate initial mass flow rate
  mdot_local=0.0_WP
  do j=jmin_,jmax_
     do i=imin_,imax_
        mdot_local=mdot_local+0.5_WP*(u(i,j,1)+u(i+1,j,1))
     end do
  end do

  call sum_real(mdot_local,mdot_init)
  
end subroutine velocity_source_init

!=======================================!
!          Velocity divergence          !
!=======================================!
subroutine velocity_divergence
  use velocity
  implicit none

  real(WP) :: div_local

  ! Reset arrays
  div_ux = 0.0_WP; div_vy = 0.0_WP; div_u = 0.0_WP

  ! Calculate divergence of u
  div_ux(imin_:imax_,:,:) = (u(imin_+1:imax_+1,:,:)-u(imin_:imax_,:,:))*dxi
  div_vy(:,jmin_:jmax_,:) = (v(:,jmin_+1:jmax_+1,:)-v(:,jmin_:jmax_,:))*dyi
  div_u = (div_ux + div_vy)*dx*dy
  call comm_borders(div_u)

  ! Total sum
  div_local = 0.0_WP
  div_local = maxval(abs(div_u(imin_:imax_,jmin_:jmax_,:)))
  call max_real(div_local,vel_divergence)

  ! Average divergence
  div_avg = div_avg + vel_divergence

  return
end subroutine velocity_divergence

!================================================!
!          Density and Viscosity Update          !
!================================================!
subroutine velocity_rhonu
  use velocity
  use math
  use variables
  use communication
  implicit none

  integer :: i,kk,ll,tt
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: rholef_buf, rhobot_buf

  ! Calculate a stochastic viscosity and density using G
  rhok = 0.0_WP; nu = 0.0_WP
  do tt = 1,ndof
     do ll = 1,ndof
        do kk = 1,ndof
           if (M(kk,ll,tt).gt.epsilon(0.0_WP)) then
              nu(:,:,tt) = nu(:,:,tt) + (nuI(kk)*G(:,:,ll)-nuE(kk)*G(:,:,ll))*M(kk,ll,tt)
              rhok(:,:,tt) = rhok(:,:,tt) + (rhoI(kk)*G(:,:,ll)-rhoE(kk)*G(:,:,ll))*M(kk,ll,tt)
           end if
        end do
     end do
     nu(:,:,tt) = nuE(tt) + nu(:,:,tt)
     rhok(:,:,tt) = rhoE(tt) + rhok(:,:,tt)
  end do
  call comm_borders(rhok); call comm_borders(nu)

  ! Calculate density at left, bottom cell face
  rho_lef = 1.0_WP; rho_bot = 1.0_WP
  rho_lef(imin_-1:imax_+1,:,:) = 0.5_WP*(rhok(imin_-1:imax_+1,:,:)+rhok(imin_-2:imax_,:,:))
  rho_bot(:,jmin_-1:jmax_+1,:) = 0.5_WP*(rhok(:,jmin_-1:jmax_+1,:)+rhok(:,jmin_-2:jmax_,:))
  call comm_borders(rho_lef); call comm_borders(rho_bot)

  ! Calculate rhoi, pressure denominator with a quadrature using rho
  rhoIk = 0.0_WP; iD_lef = 0.0_WP; iD_bot = 0.0_WP
  rhoI_denom = 0.0_WP; rholef_buf = 0.0_WP; rhobot_buf = 0.0_WP
  do tt = 1,ndof
     do i = 1,quadpts
        rholef_buf = poly_eval3(rho_lef,i)
        rhobot_buf = poly_eval3(rho_bot,i)
        rhoIk (:,:,tt) = rhoIk (:,:,tt) + Wgauss(i)*Quad_bas(tt,i)/poly_eval3(rhok,i)
        iD_lef(:,:,tt) = iD_lef(:,:,tt) + Wgauss(i)*Quad_bas(tt,i)/rholef_buf
        iD_bot(:,:,tt) = iD_bot(:,:,tt) + Wgauss(i)*Quad_bas(tt,i)/rhobot_buf
        rhoI_denom(imin_:imax_,jmin_:jmax_,tt) = rhoI_denom(imin_:imax_,jmin_:jmax_,tt) + &
             Wgauss(i)*Quad_bas(tt,i)/( &
             1.0_WP/rholef_buf(imin_+1:imax_+1,jmin_:jmax_) + &
             1.0_WP/rholef_buf(imin_  :imax_  ,jmin_:jmax_) + &
             1.0_WP/rhobot_buf(imin_:imax_,jmin_+1:jmax_+1) + &
             1.0_WP/rhobot_buf(imin_:imax_,jmin_  :jmax_  ))
     end do
     rhoIk (:,:,tt) = rhoIk (:,:,tt)/Var(tt)
     iD_lef(:,:,tt) = iD_lef(:,:,tt)/Var(tt)
     iD_bot(:,:,tt) = iD_bot(:,:,tt)/Var(tt)
     rhoI_denom(:,:,tt) = rhoI_denom(:,:,tt)/Var(tt)
  end do
  call comm_borders(rhoI_denom); call comm_borders(rhoIk)
  call comm_borders(iD_lef);     call comm_borders(iD_bot)

  ! Calculate viscosity at walls
  nu_lef = 0.0_WP; nu_bot = 0.0_WP
  nu_lef(imin_:imax_+1,:,:) = 0.5_WP*(nu(imin_-1:imax_,:,:) + nu(imin_:imax_+1,:,:))
  nu_bot(:,jmin_:jmax_+1,:) = 0.5_WP*(nu(:,jmin_-1:jmax_,:) + nu(:,jmin_:jmax_+1,:))
  call comm_borders(nu_lef); call comm_borders(nu_bot)

  return
end subroutine velocity_rhonu

!=============================================!
!          Deformation Test Velocity          !
!=============================================!
subroutine velocity_deformation
  use velocity
  use math
  implicit none
  integer :: i,j

  ! Initialize vectors
  u = 0.0_WP; v = 0.0_WP
  Uint = 0.0_WP; Vint = 0.0_WP

  ! Calculate velocity at the cell center (where x & y points match)
  do j=jmino_,jmaxo_
     do i=imino_,imaxo_
        Uint(i,j,1) = -2.0_WP*(sin(Pi*xm(i))**2.0_WP)*sin(Pi*ym(j))* &
             cos(Pi*ym(j))*cos(Pi*(time+dt)/8.0_WP)
        Vint(i,j,1) = +2.0_WP*(sin(Pi*ym(j))**2.0_WP)*sin(Pi*xm(i))* &
             cos(Pi*xm(i))*cos(Pi*(time+dt)/8.0_WP)
        ! Uint(i,j,2) = -0.08_WP*sin(Pi*xm(i))**2*sin(Pi*ym(j))* &
        !      cos(Pi*ym(j))*cos(Pi*(time+dt)/8.0_WP)
        ! Vint(i,j,2) = +0.08_WP*sin(Pi*ym(j))**2*sin(Pi*xm(i))* &
        !      cos(Pi*xm(i))*cos(Pi*(time+dt)/8.0_WP)
     end do
  end do

  ! Communicate
  call comm_borders(Uint); call comm_borders(Vint)

  ! Calculate velocity at cell walls
  u(imin_-1:imax_+2,:,:) = 0.5_WP*(Uint(imin_-2:imax_+1,:,:) + Uint(imin_-1:imax_+2,:,:))
  v(:,jmin_-1:jmax_+2,:) = 0.5_WP*(Vint(:,jmin_-2:jmax_+1,:) + Vint(:,jmin_-1:jmax_+2,:))
  call comm_borders(u); call comm_borders(v)

  return
end subroutine velocity_deformation

!===============================================!
!          Calculate Standard Velocity          !
!===============================================!
subroutine velocity_standard
  use velocity
  use variables
  implicit none

  integer :: kk,ll,tt,i
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: u_other, v_other

  ! Initialize arrays
  dPdx = 0.0_WP; dPdy = 0.0_WP
  Ucor = 0.0_WP; Vcor = 0.0_WP
  u_other = 0.0_WP; v_other = 0.0_WP

  ! Gradient of pressure at cell faces
  dPdx(imin_:imax_+1,:,:) = (P_other(imin_:imax_+1,:,:)-P_other(imin_-1:imax_,:,:))*dxi
  dPdy(:,jmin_:jmax_+1,:) = (P_other(:,jmin_:jmax_+1,:)-P_other(:,jmin_-1:jmax_,:))*dyi
  call comm_borders(dPdx); call comm_borders(dPdy)

  ! Calculate correction for test solver
  do i = 1,Mn
     kk = Mlsave(i,1); ll = Mlsave(i,2); tt = Mlsave(i,3)
     Ucor(:,:,tt) = Ucor(:,:,tt) + iD_lef(:,:,kk)*dPdx(:,:,ll)*M(kk,ll,tt)
     Vcor(:,:,tt) = Vcor(:,:,tt) + iD_bot(:,:,kk)*dPdy(:,:,ll)*M(kk,ll,tt)
  end do
  call comm_borders(Ucor); call comm_borders(Vcor)

  ! Update velocity
  u_other = ustar - dt*Ucor
  v_other = vstar - dt*Vcor
     
  ! Communicate
  call comm_borders(u_other); call comm_borders(v_other)

  ! Find difference
  u_diff = u_other - u
  v_diff = v_other - v

  return
end subroutine velocity_standard


