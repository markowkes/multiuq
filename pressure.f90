! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module pressure
  use communication
  use variables
  use parallel
  use fourier
  use basis
  use grid
  use math
  use io
  implicit none

  real(WP), dimension(:,:), pointer :: Prhs
  real(WP), dimension(:,:), pointer :: ro
  real(WP), dimension(:,:), pointer :: q
  real(WP), dimension(:,:), pointer :: s
  real(WP), dimension(:,:), pointer :: Tpr
  real(WP), dimension(:,:), pointer :: Npr

  integer :: pressure_steps_max
  real(WP) :: p_residual_max

contains 
  !=========================================================!
  !          Operator L(f) : computes Laplacian(f)          ! 
  !=========================================================!
  function L(f)
    use grid
    implicit none

    ! Inputs
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: f

    ! Variables
    integer :: kk

    ! Outputs
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: L

    ! Zero L
    L=0.0_WP

    ! Apply Neuman condition
    do kk=1,nghost
       if (rankx.eq.0   ) f(imin_-kk,:) = f(imin_,:)
       if (rankx.eq.px-1) f(imax_+kk,:) = f(imax_,:)
       if (ranky.eq.0   ) f(:,jmin_-kk) = f(:,jmin_)
       if (ranky.eq.py-1) f(:,jmax_+kk) = f(:,jmax_)
    end do

    ! Calculate laplacian of A on interior of domain
    L(imin_:imax_,jmin_:jmax_) = &
         (f(imin_-1:imax_-1,jmin_:jmax_)-2.0_WP*f(imin_:imax_,jmin_:jmax_)+f(imin_+1:imax_+1,jmin_:jmax_))*(dxi**2) + &
         (f(imin_:imax_,jmin_-1:jmax_-1)-2.0_WP*f(imin_:imax_,jmin_:jmax_)+f(imin_:imax_,jmin_+1:jmax_+1))*(dyi**2)

    return
  end function L

end module pressure

!==============================!
!       Initialization         !
!==============================!
subroutine pressure_init
  use pressure
  implicit none

  ! Allocate data
  allocate(Prhs(imino_:imaxo_,jmino_:jmaxo_))
  allocate(r   (imino_:imaxo_,jmino_:jmaxo_))
  allocate(ro  (imino_:imaxo_,jmino_:jmaxo_))
  allocate(q   (imino_:imaxo_,jmino_:jmaxo_))
  allocate(s   (imino_:imaxo_,jmino_:jmaxo_))
  allocate(Tpr (imino_:imaxo_,jmino_:jmaxo_))
  allocate(Npr (imino_:imaxo_,jmino_:jmaxo_))

  ! Allocate for test solver
  allocate(Prx_p1 (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Prx_m1 (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Pry_p1 (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Pry_m1 (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(off_Prx(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(off_Pry(imino_:imaxo_,jmino_:jmaxo_,1:ndof))

  ! Allocate for fast pressure solver
  allocate(Pr    (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(P_rhs (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Pr_old(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Pr_now(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(P_hat (imino_:imaxo_,jmino_:jmaxo_,1:ndof))

  ! Derivatives
  allocate(dPdx(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(dPdy(imino_:imaxo_,jmino_:jmaxo_,1:ndof))

  ! Density variables
  allocate(drhoIdx(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(drhoIdy(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhoX   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhoY   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhoI_denom(imino_:imaxo_,jmino_:jmaxo_,1:ndof))

  ! Fast Pressure Testing
  allocate(P_other(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(P_diff (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(P_save (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  P_other = 0.0_WP; P_diff = 0.0_WP; P_save = 0.0_WP

  ! Inialize Variables
  Pr   = 0.0_WP; P_rhs = 0.0_WP; Pr_old = 0.0_WP
  dPdx = 0.0_WP; dPdy  = 0.0_WP; Pr_now = 0.0_WP

  ! Find minimum density
  rho_0 = min(rhoI(1),rhoE(1))

  ! Initialize pressure iteration tally
  pressure_iterations = 0

  ! Initialize hypre library
  if (use_HYPRE) call hypre_init

  ! Initialize FFT
  if (use_FFTW) call fourier_init

  return
end subroutine pressure_init

!==========================================!
!          Fast Fourier Transform          !
!==========================================!
subroutine pressure_fft(dd)
  use pressure
  implicit none

  integer, intent(in) :: dd
  ! real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: Ff, Sf
  ! real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: Gf, Xf
  ! real(WP), dimension(imino_:imaxo_) :: Sigmaf
  ! real(WP) :: h
  ! integer :: i,j

  ! ! First implement a simple fast poisson solver
  ! h = 1.0_WP/(1.0_WP+real(nx,WP))
  ! Ff = 0.0_WP; Sf = 0.0_WP; Gf = 0.0_WP; Xf = 0.0_WP; Sigmaf = 0.0_WP

  ! ! Algorithm
  ! Ff(imin_:imax_,jmin_:jmax) = -Prhs(imin_:imax_,jmin_:jmax)
  ! do i = imin_,imax_
  !    do j = jmin_,jmax_
  !       Sf(i,j) = sin(real(i*j,WP)*pi*h)
  !    end do
  !    Sigmaf(i) = sin(real(i,WP)*pi*h*0.5_WP)**2
  ! end do
  ! Gf = matmul(matmul(Sf,Ff),Sf)
  ! do i = imin_,imax_
  !    do j = jmin_,jmax_
  !       Xf(i,j) = (h**4)*Gf(i,j)/(Sigmaf(i)+Sigmaf(j))
  !    end do
  ! end do
  ! Pr(:,:,dd) = matmul(matmul(Sf,Xf),Sf)

  ! Use fftw package
  call fourier_transform(Prhs(imin_:imax_,jmin_:jmax_))
  call fourier_inverse(Prhs(imin_:imax_,jmin_:jmax_))
  Pr(:,:,dd) = 0.0_WP
  Pr(imin_:imax_,jmin_:jmax,dd) = Prhs(imin_:imax_,jmin_:jmax_)

  ! if (rank.eq.root) print*, 'Max diff = ',maxval(abs(Xf-Pr(:,:,dd)))

end subroutine pressure_fft

!=================================================!
!          Stabalized Conjugate Gradient          !
!=================================================!
subroutine pressure_bicgstab(dd)
  use pressure
  implicit none

  integer, intent(in) :: dd
  real(WP) :: rhnew, rhold, alpha, omega, beta

  ! Initialize variables
  Pr(:,:,dd) = 0.0_WP
  Tpr   = 0.0_WP
  ro    = Prhs ! = b-L(p)
  r     = ro
  rhold = 1.0_WP
  alpha = 1.0_WP
  omega = 1.0_WP
  q     = 0.0_WP
  Npr   = 0.0_WP

  ! Initialize loop
  pressure_steps = 0
  p_res = sqrt(AtB(r,r))*dx*dy

  do while ((p_res.gt.cvg_criteria).and.(pressure_steps.lt.pressure_max_steps))
     pressure_steps = pressure_steps+1
     rhnew = AtB(ro,r)
     beta = (rhnew/rhold)*(alpha/omega)
     q=ApBC(r,beta,ApBC(q,-1.0_WP*omega,Npr))
     call P_comm_borders_2d(q)
     Npr = L(q)
     alpha = AtB(ro,Npr)
     alpha = rhnew/(sign(1.0_WP,alpha)*max(abs(alpha),epsilon(1.0_WP)))
     s = ApBC(r,-1.0_WP*alpha,Npr)
     call P_comm_borders_2d(s)
     Tpr = L(s)
     omega = AtB(Tpr,Tpr)
     omega = AtB(Tpr,s)/(sign(1.0_WP,omega)*max(abs(omega),epsilon(1.0_WP)))

     ! Update the pressure field
     Pr(:,:,dd) = ApBC(ApBC(Pr(:,:,dd),alpha,q),omega,s)
     call P_comm_borders_2d(Pr(:,:,dd))

     ! Save information for next iteration
     r = ApBC(s,-1.0_WP*omega,Tpr)
     rhold = sign(1.0_WP,rhnew)*max(abs(rhnew),epsilon(0.0_WP))
     p_res = sqrt(AtB(r,r))*dx*dy
  end do

end subroutine pressure_bicgstab

!=======================================!
!          Gauss-Seidel Solver          !
!=======================================!
subroutine pressure_test
  use pressure
  implicit none

  integer :: kk,ll,tt,verr,i
  real(WP) :: p_res_local

  ! Initialize arrays
  div_ux = 0.0_WP; div_vy = 0.0_WP; div_u = 0.0_WP
  Prx_p1 = 0.0_WP; Prx_m1 = 0.0_WP; P_rhs = 0.0_WP
  Pry_p1 = 0.0_WP; Pry_m1 = 0.0_WP

  ! Calculate divergence of ustar term
  div_ux(imin_:imax_,:,:) = (ustar(imin_+1:imax_+1,:,:)-ustar(imin_:imax_,:,:))*dxi
  div_vy(:,jmin_:jmax_,:) = (vstar(:,jmin_+1:jmax_+1,:)-vstar(:,jmin_:jmax_,:))*dyi
  div_u = (div_ux*(dx**2) + div_vy*(dy**2))/dt
  call comm_borders(div_u)

  ! Calculate pieces that don't change
  do i = 1,Mn
     kk = Mlsave(i,1); ll = Mlsave(i,2); tt = Mlsave(i,3)
     P_rhs(:,:,tt) = P_rhs(:,:,tt) + div_u(:,:,kk)*rhoI_denom(:,:,ll)*M(kk,ll,tt)
     Prx_p1(imin_:imax_,:,tt) = Prx_p1(imin_:imax_,:,tt) + &
          iD_lef(imin_+1:imax_+1,:,kk)*rhoI_denom(imin_:imax_,:,ll)*M(kk,ll,tt)
     Prx_m1(imin_:imax_,:,tt) = Prx_m1(imin_:imax_,:,tt) + &
          iD_lef(imin_:imax_,:,kk)*rhoI_denom(imin_:imax_,:,ll)*M(kk,ll,tt)
     Pry_p1(:,jmin_:jmax_,tt) = Pry_p1(:,jmin_:jmax_,tt) + &
          iD_bot(:,jmin_+1:jmax_+1,kk)*rhoI_denom(:,jmin_:jmax_,ll)*M(kk,ll,tt)
     Pry_m1(:,jmin_:jmax_,tt) = Pry_m1(:,jmin_:jmax_,tt) + &
          iD_bot(:,jmin_:jmax_,kk)*rhoI_denom(:,jmin_:jmax_,ll)*M(kk,ll,tt)
  end do
  call comm_borders(Prx_p1); call comm_borders(Prx_m1); call comm_borders(P_rhs)
  call comm_borders(Pry_p1); call comm_borders(Pry_m1)

  ! Iterate until convergence is reached
  Pr_old = 1.0_WP; p_res = 1.0_WP; pressure_steps = 0
  do while ((p_res.gt.cvg_criteria).and.(pressure_steps.lt.pressure_max_steps))
     pressure_steps = pressure_steps + 1
     pressure_iterations = pressure_iterations + 1

     ! Calculate off terms
     off_Prx = 0.0_WP; off_Pry = 0.0_WP
     do i = 1,Mn
        kk = Mlsave(i,1); ll = Mlsave(i,2); tt = Mlsave(i,3)
        off_Prx(imin_:imax_,:,tt) = off_Prx(imin_:imax_,:,tt) + &
             (Pr(imin_-1:imax_-1,:,kk)*Prx_m1(imin_:imax_,:,ll) + &
             Pr(imin_+1:imax_+1,:,kk)*Prx_p1(imin_:imax_,:,ll))*M(kk,ll,tt)
        off_Pry(:,jmin_:jmax_,tt) = off_Pry(:,jmin_:jmax_,tt) + &
             (Pr(:,jmin_-1:jmax_-1,kk)*Pry_m1(:,jmin_:jmax_,ll) + &
             Pr(:,jmin_+1:jmax_+1,kk)*Pry_p1(:,jmin_:jmax_,ll))*M(kk,ll,tt)
     end do

     ! Update Pressure
     Pr = off_Prx + off_Pry - P_rhs

     ! Communicate
     call P_comm_borders(Pr)

     ! Apply Neuman conditions
     do kk=1,nghost
        if (rankx.eq.0   ) Pr(imin_-kk,:,:) = Pr(imin_,:,:)
        if (rankx.eq.px-1) Pr(imax_+kk,:,:) = Pr(imax_,:,:)
        if (ranky.eq.0   ) Pr(:,jmin_-kk,:) = Pr(:,jmin_,:)
        if (ranky.eq.py-1) Pr(:,jmax_+kk,:) = Pr(:,jmax_,:)
     end do
     call MPI_BARRIER(COMM, verr)

     ! Update residual and save old pressure
     p_res_local = maxval(abs(Pr-Pr_old))
     call max_real(p_res_local,p_res)
     Pr_old = Pr

  end do

end subroutine pressure_test

!==================================================!
!          Fast Pressure Correction Method         !
!==================================================!
subroutine pressure_fast
  use pressure
  implicit none

  integer :: kk,ll,tt
  integer :: i,verr
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: drhodx,drhody

  ! Initialize arrays
  div_ux = 0.0_WP; div_vy = 0.0_WP; div_u = 0.0_WP
  drhodx = 0.0_WP; drhody = 0.0_WP; P_rhs = 0.0_WP
  dPdx   = 0.0_WP; dPdy   = 0.0_WP

  ! Calculate divergence of ustar at cell center
  div_ux(imin_:imax_,:,:) = (ustar(imin_+1:imax_+1,:,:)-ustar(imin_:imax_,:,:))*dxi
  div_vy(:,jmin_:jmax_,:) = (vstar(:,jmin_+1:jmax_+1,:)-vstar(:,jmin_:jmax_,:))*dyi
  div_u = rho_0*(div_ux + div_vy)/dt
  call comm_borders(div_u)

  ! Loop over pressure poisson equation
  ctr3 = 0; Phat_res = 1.0_WP
  do while ((Phat_res.gt.cvg_criteria).and.(ctr3.lt.pei))
     P_hat = Pr; rhoX = 0.0_WP; rhoY = 0.0_WP
     ctr3 = ctr3 + 1

     ! Calculate gradient of pressure approximation at cell walls
     dPdx(imin_:imax_+1,:,:) = (P_hat(imin_:imax_+1,:,:)-P_hat(imin_-1:imax_,:,:))*dxi
     dPdy(:,jmin_:jmax_+1,:) = (P_hat(:,jmin_:jmax_+1,:)-P_hat(:,jmin_-1:jmax_,:))*dyi
     call comm_borders(dPdx); call comm_borders(dPdy)

     ! Compile density and pressure approximation at cell walls
     do i = 1,Mn
        kk = Mlsave(i,1); ll = Mlsave(i,2); tt = Mlsave(i,3)
        rhoX(:,:,tt) = rhoX(:,:,tt) + rho_0*iD_lef(:,:,kk)*dPdx(:,:,ll)*M(kk,ll,tt)
        rhoY(:,:,tt) = rhoY(:,:,tt) + rho_0*iD_bot(:,:,kk)*dPdy(:,:,ll)*M(kk,ll,tt)
     end do
     rhoX = dPdx - rhoX; rhoY = dPdy - rhoY
     call comm_borders(rhoX); call comm_borders(rhoY)

     ! Compile rhs of fast pressure solver at cell center
     drhodx(imin_:imax_,:,:) = (rhoX(imin_+1:imax_+1,:,:)-rhoX(imin_:imax_,:,:))*dxi
     drhody(:,jmin_:jmax_,:) = (rhoY(:,jmin_+1:jmax_+1,:)-rhoY(:,jmin_:jmax_,:))*dyi
     P_rhs = div_u + drhodx + drhody
     call comm_borders(P_rhs)

     ! Use pressure solution algorithm
     do kk = 1,ndof
        Prhs = P_rhs(:,:,kk)
        if (use_HYPRE) then
           call pressure_hypre(kk)
        else
           call pressure_bicgstab(kk)
        end if
        pressure_iterations = pressure_iterations + pressure_steps
     end do

     ! Communicate
     call P_comm_borders(Pr)

     ! Apply Neuman conditions
     do kk=1,nghost
        if (rankx.eq.0   ) Pr(imin_-kk,:,:) = Pr(imin_,:,:)
        if (rankx.eq.px-1) Pr(imax_+kk,:,:) = Pr(imax_,:,:)
        if (ranky.eq.0   ) Pr(:,jmin_-kk,:) = Pr(:,jmin_,:)
        if (ranky.eq.py-1) Pr(:,jmax_+kk,:) = Pr(:,jmax_,:)
     end do

     ! Wait for all processes
     call MPI_BARRIER(COMM, verr)

     ! Check difference
     myPhat_res = maxval(abs(P_hat(imin_:imax_,jmin_:jmax_,1:ndof)-Pr(imin_:imax_,jmin_:jmax_,1:ndof)))
     call max_real(myPhat_res,Phat_res)
  end do

end subroutine pressure_fast

!======================================================!
!          Semi-Lagrangian Pressure Projection         !
!======================================================!
subroutine pressure_phat
  use pressure
  implicit none

  integer :: i,j,ii
  integer :: kk, ll, mm, tt
  integer :: verr
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: offx, offy
  ! real(WP), dimension(1,2) :: x_diff
  ! real(WP), dimension(2,1) :: y_diff
  ! real(WP), dimension(2,2) :: P_points
  ! real(WP), dimension(1,1) :: store
  ! integer :: i_find, j_find

  ! Calculate cell centered velocities
  Uint = 0.0_WP; Vint = 0.0_WP
  Uint(imin_:imax_,:,:) = 0.5_WP*(u(imin_:imax_,:,:) + u(imin_+1:imax_+1,:,:))
  Vint(:,jmin_:jmax_,:) = 0.5_WP*(v(:,jmin_:jmax_,:) + v(:,jmin_+1:jmax_+1,:))

  ! ! Back track to old particle location
  ! offx = 0.0_WP; offy = 0.0_WP
  ! do i = imin_,imax_
  !    do j = jmin_,jmax_
  !       do kk = 1,ndof
  !          offx(i,j,kk) = xm(i)*Bas(kk) - dt*Uint(i,j,kk)
  !          offy(i,j,kk) = ym(j)*Bas(kk) - dt*Vint(i,j,kk)
  !       end do
  !    end do
  ! end do

  ! ! Perform bilinear interpolation to find pressure at old particle
  ! do i = imin_,imax_
  !    do j = jmin_,jmax_
  !       do kk = 1,ndof
  !          ! Find quadrant where old point lies
  !          i_find = 1; j_find = 1;
  !          if (offx(i,j,1).lt.xm(i)) i_find = -1
  !          if (offy(i,j,1).lt.ym(j)) j_find = -1
  !          ! Populate matrices
  !          x_diff(1,1) = xm(i)*Bas(kk) - offx(i,j,kk); x_diff(1,2) = offx(i,j,kk) - xm(i+i_find)*Bas(kk)
  !          y_diff(1,1) = ym(j)*Bas(kk) - offy(i,j,kk); y_diff(2,1) = offy(i,j,kk) - ym(j+j_find)*Bas(kk)
  !          P_points(1,1) = Pr(i+i_find,j+j_find,kk); P_points(2,1) = Pr(i,j+j_find,kk)
  !          P_points(1,2) = Pr(i+i_find,j,kk);        P_points(2,2) = Pr(i,j,kk)
  !          ! ! Use a wide block
  !          ! x_diff(1,1) = xm(i+1) - offx(i,j,1); x_diff(1,2) = offx(i,j,1) - xm(i-1)
  !          ! y_diff(1,1) = ym(j+1) - offy(i,j,1); y_diff(2,1) = offy(i,j,1) - ym(j-1)
  !          ! P_points(1,1) = Pr(i-1,j-1,kk); P_points(2,1) = Pr(i+1,j-1,kk)
  !          ! P_points(1,2) = Pr(i-1,j+1,kk); P_points(2,2) = Pr(i+1,j+1,kk)
  !          ! Find pressure
  !          store = P_denom*matmul(matmul(x_diff,P_points),y_diff)
  !          P_hat(i,j,kk) = store(1,1)
  !       end do
  !    end do
  ! end do

  ! Stochastic interpolation (on wide grid)
  ! Find uncertain particle location
  offx = 0.0_WP; offy = 0.0_WP; P_hat = 0.0_WP
  do i = imin_,imax_
     do j = jmin_,jmax_
        do kk = 1,ndof
           offx(i,j,kk) = xm(i)*Bas(kk) - dt*Uint(i,j,kk)
           offy(i,j,kk) = ym(j)*Bas(kk) - dt*Vint(i,j,kk)
           ! Part one of broken up interpolation
           P_hat(i,j,kk) = P_hat(i,j,kk) + &
                Pr(i-1,j-1,kk)*xm(i+1)*ym(j+1) - Pr(i-1,j+1,kk)*xm(i+1)*ym(j-1) - &
                Pr(i+1,j-1,kk)*xm(i-1)*ym(j+1) + Pr(i+1,j+1,kk)*xm(i-1)*ym(j-1)
        end do
        ! Part two
        do ii = 1,Mn
           kk = Mlsave(ii,1); ll = Mlsave(ii,2); tt = Mlsave(ii,3)
           P_hat(i,j,tt) = P_hat(i,j,tt) + offx(i,j,kk)*( &
                -Pr(i-1,j-1,ll)*ym(j+1) + Pr(i-1,j+1,ll)*ym(j-1) &
                +Pr(i+1,j-1,ll)*ym(j+1) - Pr(i+1,j+1,ll)*ym(j-1))*M(kk,ll,tt) &
                + offy(i,j,kk)*( &
                -Pr(i-1,j-1,ll)*xm(i+1) + Pr(i-1,j+1,ll)*xm(i+1) &
                +Pr(i+1,j-1,ll)*xm(i-1) - Pr(i+1,j+1,ll)*xm(i-1))*M(kk,ll,tt)
        end do
     end do
  end do
  ! Part three
  do ii = 1,Dn
     kk = Dlsave(ii,1); ll = Dlsave(ii,2); mm = Dlsave(ii,3); tt = Dlsave(ii,4)
     P_hat(imin_:imax_,jmin_:jmax_,tt) = P_hat(imin_:imax_,jmin_:jmax_,tt) + &
          offx(imin_:imax_,jmin_:jmax_,kk)*offy(imin_:imax_,jmin_:jmax_,ll)*(&
          Pr(imin_-1:imax_-1,jmin_-1:jmax_-1,mm) - Pr(imin_-1:imax_-1,jmin_+1:jmax_+1,mm) - &
          Pr(imin_+1:imax_+1,jmin_-1:jmax_-1,mm) + Pr(imin_+1:imax_+1,jmin_+1:jmax_+1,mm))* &
          Dklmt(kk,ll,mm,tt)
  end do

  ! Communicate
  Pr = P_hat*0.25_WP*dxi*dyi
  call P_comm_borders(Pr)

  ! Apply Neuman conditions
  do kk = 1,nghost
     if (rankx.eq.0   ) Pr(imin_-kk,:,:) = Pr(imin_,:,:)
     if (rankx.eq.px-1) Pr(imax_+kk,:,:) = Pr(imax_,:,:)
     if (ranky.eq.0   ) Pr(:,jmin_-kk,:) = Pr(:,jmin_,:)
     if (ranky.eq.py-1) Pr(:,jmax_+kk,:) = Pr(:,jmax_,:)
  end do

  ! Wait for all processes
  call MPI_BARRIER(COMM, verr)

end subroutine pressure_phat

!=====================================================!
!          Shepard's Method for Interpolation         !
!=====================================================!
subroutine pressure_shepard
  use pressure
  implicit none

  integer :: i,j,kk
  integer :: ii, jj
  integer :: verr
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: offx, offy
  real(WP), dimension(-1:1,-1:1) :: P_dist
  real(WP) :: x_loc, y_loc

  ! Calculate cell centered velocities
  Uint = 0.0_WP; Vint = 0.0_WP
  Uint(imin_:imax_,:,:) = 0.5_WP*(u(imin_:imax_,:,:) + u(imin_+1:imax_+1,:,:))
  Vint(:,jmin_:jmax_,:) = 0.5_WP*(v(:,jmin_:jmax_,:) + v(:,jmin_+1:jmax_+1,:))

  ! Back track to old particle location (average location) & find distances
  offx = 0.0_WP; offy = 0.0_WP
  do i = imin_,imax_
     do j = jmin_,jmax_
        x_loc = xm(i) - dt*Uint(i,j,1)
        y_loc = ym(j) - dt*Vint(i,j,1)
        do ii = -1,1
           do jj = -1,1
              P_dist(ii,jj) = 1.0_WP/(sqrt((xm(i+ii)-x_loc)**2 + (ym(j+jj)-y_loc)**2)**3)
           end do
        end do
        P_hat(i,j,1) = sum(Pr(i-1:i+1,j-1:j+1,1)*P_dist)/sum(P_dist)
     end do
  end do

  ! Communicate
  P_hat = Pr
  call P_comm_borders(Pr)

  ! Apply Neuman conditions
  do kk = 1,nghost
     if (rankx.eq.0   ) Pr(imin_-kk,:,:) = Pr(imin_,:,:)
     if (rankx.eq.px-1) Pr(imax_+kk,:,:) = Pr(imax_,:,:)
     if (ranky.eq.0   ) Pr(:,jmin_-kk,:) = Pr(:,jmin_,:)
     if (ranky.eq.py-1) Pr(:,jmax_+kk,:) = Pr(:,jmax_,:)
  end do

  ! Wait for all processes
  call MPI_BARRIER(COMM, verr)

end subroutine pressure_shepard

!=======================================================!
!          Use HYPRE package to solve pressure          !
!=======================================================!
subroutine pressure_hypre(dd)
  use pressure
  implicit none

  integer, intent(in) :: dd

  ! Initialize counter
  pressure_steps = 0

  ! Transfer data to HYPRE
  call hypre_in(dd)

  ! Solve with hypre
  call hypre_solve

  ! Transfer data back to multiUQ
  call hypre_out(dd)


end subroutine pressure_hypre









