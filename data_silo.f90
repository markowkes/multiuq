! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!================================!
! Read Data from Visit Silo file !
!================================!
module data_silo
  use variables
  use fortransilo
  use grid
  use visit
  implicit none

  ! File to read (should be added to inputs)
  character(len=30) :: dirname

  ! Multi-meshes
  integer :: mmesh_n
  character(len=:), pointer :: mmesh_names(:)
  real(WP), dimension(:), allocatable :: mmesh_xmin, mmesh_xmax
  real(WP), dimension(:), allocatable :: mmesh_ymin, mmesh_ymax
  
  ! Meshes
  real(WP), pointer :: qmesh_x(:) => null()
  real(WP), pointer :: qmesh_y(:) => null()
  real(WP), pointer :: qmesh_z(:) => null()
  integer, pointer :: qmesh_dims(:) => null()
  integer :: qmesh_ndims


  
end module data_silo

!======================================!
!          Read Silo file              !
!======================================!
subroutine data_silo_read
  use data_silo
  implicit none

  character(len=11) :: buffer

  ! Read time to restart at
  call read_input('Restart time',restart_time)

  ! Silo file to read
  write(buffer,'(ES11.5)') restart_time
  dirname = 'Visit_restart/time_'//buffer

  ! Read mesh and data
  call data_silo_read_mmesh
  call data_silo_read_mesh ! Calls data_silo_read_vars

  return
end subroutine data_silo_read


!======================================!
!          Read multi-mesh             !
!======================================!
subroutine data_silo_read_mmesh
  use data_silo
  implicit none
   
  character(len=41) :: filename
  character(len=20) :: mesh='Mesh'
  integer, pointer :: mmeshtypes(:)
  integer :: err,dbid,optlistID
  integer :: n,count,extentssize
  real(WP) :: mmesh_time
  real(WP), pointer :: extents(:)

  ! Filename of multimesh
  filename=dirname//'/Visit.silo'
  
  ! Open database
  err = dbopen(filename, len(trim(filename)), DB_HDF5, DB_READ, dbid)
  if (err.ne.0) call die('Can''t open silo file '//dirname//' for restart')
  
  ! Read multi-mesh
  err = dbgetmmesh(dbid,mesh,mmesh_n,mmesh_names,mmeshtypes,optlistID)

  ! Read & set time
  err = dbgetvar(dbid, 'dtime', mmesh_time)
  time=mmesh_time

  ! Read the extents of each mesh
  err = dbgetopt(optlistID, DBOPT_EXTENTS_SIZE, extentssize)
  allocate(extents(extentssize*mmesh_n))
  err = dbgetopt(optlistID, DBOPT_EXTENTS, extents)

  ! Extract bounds from extents
  allocate(mmesh_xmin(mmesh_n))
  allocate(mmesh_xmax(mmesh_n))
  allocate(mmesh_ymin(mmesh_n))
  allocate(mmesh_ymax(mmesh_n))
  count=0
  do n=1,mmesh_n
     mmesh_xmin(n)=extents(count+1)
     mmesh_ymin(n)=extents(count+2)
     mmesh_xmax(n)=extents(count+3)
     mmesh_ymax(n)=extents(count+4)
     count=count+4
  end do
  
  ! Close database
  err = dbclose(dbid)
 
  return
end subroutine data_silo_read_mmesh


!========================!
!       Read mesh        !
!========================!
subroutine data_silo_read_mesh
  use data_silo

  implicit none
  
  character(len=100) :: filename,siloname,dir_name
  integer :: index
  character(len=20) :: mesh='Mesh'
  integer :: err,dbid
  integer :: n,extentssize

  character(len=:), pointer :: qmesh_xname
  character(len=:), pointer :: qmesh_yname
  character(len=:), pointer :: qmesh_zname
  integer :: qmesh_datatype
  integer :: qmesh_coordtype
  integer :: qmesh_optlist

  integer :: i,j

  ! Loop over meshes
  do n=1,mmesh_n

     ! Check if this mesh overlaps this procs domain partition
     if (.not. ( &
          mmesh_xmin(n).lt.x(imax_+1)-epsilon(0.0_WP) .and. &
          mmesh_xmax(n).gt.x(imin_  )+epsilon(0.0_WP) .and. &
          mmesh_ymin(n).lt.y(jmax_+1)-epsilon(0.0_WP) .and. &
          mmesh_ymax(n).gt.y(jmin_  )+epsilon(0.0_WP) ) ) then
        cycle
     end if

     ! Get filename
     filename=dirname//'/'//mmesh_names(n)

     ! Split filename into silo name and internal directory
     index = scan(filename,':')
     siloname=filename(1:index-1)
     dir_name=filename(index+1:)
     index = scan(dir_name,'/')
     dir_name=dir_name(1:index-1)

     ! Open database
     err = dbopen(siloname, len(trim(siloname)), DB_HDF5, DB_READ, dbid)
     if (err.ne.0) call die('Can''t open silo file to read for restart')

     ! Move into directory
     err = dbsetdir(dbid,dir_name,len(trim(dir_name)))
     if (err.ne.0) call die('Can''t move into silo directory within '//siloname)

     ! Read the mesh
     err = dbgetqm(dbid,"Mesh", qmesh_xname, qmesh_yname, qmesh_zname, &
          qmesh_x, qmesh_y, qmesh_z, qmesh_dims, &
          qmesh_ndims, qmesh_datatype, qmesh_coordtype, qmesh_optlist)

     ! Check that the mesh is double precision
     if (qmesh_datatype.ne.DB_DOUBLE) &
          call die('Mesh data in silo file to read is not double precision')
     ! Check number of dimensions
     if (qmesh_ndims.ne.2) &
          call die('Silo file to read is not two-dimensional')     


     ! Read data from silo file
     call data_silo_read_vars(dbid)

     ! Close database
     err = dbclose(dbid)
        
  end do
  
  return
end subroutine data_silo_read_mesh



!========================!
!       Read vars        !
!========================!
subroutine data_silo_read_vars(dbid)
  use data_silo
  implicit none
  integer, intent(in) :: dbid
  integer :: err  
  character(len=:), pointer :: qvar_meshname
  integer :: qvar_nvars
  character(len=:), pointer :: qvar_varnames(:)
  integer, pointer :: qvar_dims(:) => null()
  integer :: qvar_ndims
  character, pointer ::qvar_mixvars
  integer, pointer ::qvar_mixlen(:) => null()
  integer ::qvar_datatype
  integer ::qvar_centering
  integer ::qvar_optlist

  integer :: i,j,k
  integer :: qmesh_imin,qmesh_imax,qmesh_jmin,qmesh_jmax
  integer ::  olap_imin, olap_imax, olap_jmin, olap_jmax

  ! Identify indices of overlap
  ! ---------------------------
  ! Left edge
  if (qmesh_x(1).lt.x(imin_)) then ! find index on silo qmesh that matches x(imin_)
     olap_imin=imin_
     do i=1,qmesh_dims(1)
        if (abs(qmesh_x(i)-x(imin_  )).lt.epsilon(0.0_WP)) then
           qmesh_imin=i
           exit
        end if
     end do
  else ! find index on proc mesh that matches qmesh_x(1)
     qmesh_imin=1
     do i=imin_,imax_+1
        if (abs(qmesh_x(1)-x(i)).lt.epsilon(0.0_WP)) then
           olap_imin=i
           exit
        end if
     end do
  end if

  ! Right edge
  if (qmesh_x(qmesh_dims(1)).gt.x(imax_+1)) then ! find index on silo qmesh that matches x(imin_)
     olap_imax=imax_
     do i=qmesh_dims(1),1,-1
        if (abs(qmesh_x(i)-x(imax_+1)).lt.epsilon(0.0_WP)) then
           qmesh_imax=i-1
           exit
        end if
     end do
  else ! find index on proc mesh that matches qmesh_x(qmesh_dims(1))
     qmesh_imax=qmesh_dims(1)-1
     do i=imax_+1,imin_,-1
        if (abs(qmesh_x(qmesh_dims(1))-x(i)).lt.epsilon(0.0_WP)) then
           olap_imax=i-1
           exit
        end if
     end do
  end if
  
  ! Bottom edge
  if (qmesh_y(1).lt.y(jmin_)) then ! find index on silo qmesh that matches y(jmin_)
     olap_jmin=jmin_
     do j=1,qmesh_dims(2)
        if (abs(qmesh_y(j)-y(jmin_  )).lt.epsilon(0.0_WP)) then
           qmesh_jmin=j
           exit
        end if
     end do
  else ! find index on proc mesh that matches qmesh_y(1)
     qmesh_jmin=1
     do j=jmin_,jmax_+1
        if (abs(qmesh_y(1)-y(j)).lt.epsilon(0.0_WP)) then
           olap_jmin=j
           exit
        end if
     end do
  end if

  ! Top edge
  if (qmesh_y(qmesh_dims(2)).gt.y(jmax_+1)) then ! find index on silo qmesh that matches y(jmin_)
     olap_jmax=jmax_
     do j=qmesh_dims(2),1,-1
        if (abs(qmesh_y(j)-y(jmax_+1)).lt.epsilon(0.0_WP)) then
           qmesh_jmax=j-1
           exit
        end if
     end do
  else ! find index on proc mesh that matches qmesh_y(qmesh_dims(2))
     qmesh_jmax=qmesh_dims(2)-1
     do j=jmax_+1,jmin_,-1
        if (abs(qmesh_y(qmesh_dims(2))-y(j)).lt.epsilon(0.0_WP)) then
           olap_jmax=j-1
           exit
        end if
     end do
  end if
  
  ! Read data
  do k=1,ndof
     call read_silo_var( 'u',k, u(olap_imin:olap_imax,olap_jmin:olap_jmax,k))
     call read_silo_var( 'v',k, v(olap_imin:olap_imax,olap_jmin:olap_jmax,k))
     call read_silo_var( 'G',k, G(olap_imin:olap_imax,olap_jmin:olap_jmax,k))
     call read_silo_var('Pr',k,Pr(olap_imin:olap_imax,olap_jmin:olap_jmax,k))
  end do
  
  return

contains
  subroutine read_silo_var(name,dof,values)
    character(len=*), intent(in) :: name
    integer, intent(in) :: dof
    real(WP), pointer :: qvar_vars(:,:,:) => null()
    real(WP) :: values(:,:)
    character(len=str_medium) :: med_buffer
    
    write(med_buffer,'(A,I0.3)') name,dof
    
    err = dbgetqv(dbid, med_buffer, qvar_meshname, qvar_nvars, qvar_varnames, &
         & qvar_vars, qvar_dims, qvar_ndims, qvar_mixvars, qvar_mixlen, &
         & qvar_datatype,qvar_centering,qvar_optlist)

    ! Check for error reading var
    if (err.ne.0) call die('Can''t read '//trim(med_buffer)//' from silo file')

    ! Check datatype
    if (qvar_datatype.ne.DB_DOUBLE) &
         call die('Data in silo file to read is not double precision')

    ! Check number of dimensions
    if (size(qvar_dims).ne.2) &
         call die('Silo file to read is not two-dimensional')

    ! Transfer values to 2D array
    values = qvar_vars(qmesh_imin:qmesh_imax,qmesh_jmin:qmesh_jmax,0)

  end subroutine read_silo_var
  
end subroutine data_silo_read_vars
