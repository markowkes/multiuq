! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!================================!
!      Write and Read Data       !
!================================!
module data
  use variables
  use basis
  use grid
  use levelset
  use parallel
  use velocity
  implicit none


end module data

!===================================================!
!          Write a file for each processor          !
!===================================================!
subroutine data_write_serial
  use data
  use variables
  implicit none

  character(len=str_medium) :: G_f, P_f, u_f, v_f
  character(len=str_medium) :: dirname, buffer
  character(len=str_medium) :: px_buf, py_buf
  logical :: file_exists
  integer :: verr

  ! Create the file names
  write(px_buf,'(I6)') rankx
  write(py_buf,'(I6)') ranky
  G_f = 'G_'//trim(px_buf)//'_'//trim(py_buf)
  P_f = 'P_'//trim(px_buf)//'_'//trim(py_buf)
  u_f = 'u_'//trim(px_buf)//'_'//trim(py_buf)
  v_f = 'v_'//trim(px_buf)//'_'//trim(py_buf)

  ! Add info to directory name
  ! This will automatically overwrite old data
  select case(trim(filext))
  case ('iter')
     write(buffer,'(I6)') iter
     dirname = 'Data/iter_'//trim(adjustl(buffer))

  case ('time')
     write(buffer,'(ES12.3)') time
     dirname = 'Data/time_'//trim(adjustl(buffer))

  case default
     call die('Unknown File Extension Type: '//trim(filext))
     print*, 'Choose iter or time'
  end select

  ! Create new directory
  if (rank.eq.root) call system('mkdir -p '// adjustl(trim(dirname)))
  call MPI_BARRIER(COMM, verr)

  ! Write to binary file
  ! Open level set
  inquire(file=trim(dirname)//'/'//trim(G_f),exist=file_exists)
  if (file_exists) then
     open(unit = 10, status='replace',action="write",file=trim(dirname)//'/'//trim(G_f),form='unformatted')
  else
     open(unit = 10, status='new',action="write",file=trim(dirname)//'/'//trim(G_f),form='unformatted')
  end if
  ! Open pressure
  inquire(file=trim(dirname)//'/'//trim(P_f),exist=file_exists)
  if (file_exists) then
     open(unit = 20, status='replace',action="write",file=trim(dirname)//'/'//trim(P_f),form='unformatted')
  else
     open(unit = 20, status='new',action="write",file=trim(dirname)//'/'//trim(P_f),form='unformatted')
  end if
  ! Open u velocity
  inquire(file=trim(dirname)//'/'//trim(u_f),exist=file_exists)
  if (file_exists) then
     open(unit = 30, status='replace',action="write",file=trim(dirname)//'/'//trim(u_f),form='unformatted')
  else
     open(unit = 30, status='new',action="write",file=trim(dirname)//'/'//trim(u_f),form='unformatted')
  end if
  ! Open v velocity
  inquire(file=trim(dirname)//'/'//trim(v_f),exist=file_exists)
  if (file_exists) then
     open(unit = 40, status='replace',action="write",file=trim(dirname)//'/'//trim(v_f),form='unformatted')
  else
     open(unit = 40, status='new',action="write",file=trim(dirname)//'/'//trim(v_f),form='unformatted')
  end if

  ! Write data into the file
  write(10) G (imin_:imax_  ,jmin_:jmax_  ,1:ndof); close(10)
  write(20) Pr(imin_:imax_  ,jmin_:jmax_  ,1:ndof); close(20)
  write(30) u (imin_:imax_+1,jmin_:jmax_  ,1:ndof); close(30)
  write(40) v (imin_:imax_  ,jmin_:jmax_+1,1:ndof); close(40)

end subroutine data_write_serial

!================================================!
!          Read files to each processor          !
!================================================!
subroutine data_read_serial
  use data
  use variables
  use io
  implicit none

  ! Define Necessary Variables
  character(len=str_medium) :: G_f, P_f, u_f, v_f
  character(len=str_medium) :: dirname, buffer
  character(len=str_medium) :: px_buf, py_buf
  integer :: stit

  ! Create the file names
  write(px_buf,'(I6)') rankx
  write(py_buf,'(I6)') ranky
  G_f = 'G_'//trim(px_buf)//'_'//trim(py_buf)
  P_f = 'P_'//trim(px_buf)//'_'//trim(py_buf)
  u_f = 'u_'//trim(px_buf)//'_'//trim(py_buf)
  v_f = 'v_'//trim(px_buf)//'_'//trim(py_buf)

  ! Add time info to directory name
  ! This will automatically overwrite old data
  select case(trim(filext))
  case ('iter')
     call read_input('Starting Iteration',stit)
     write(buffer,'(I6)') stit
     dirname = 'Data/iter_'//trim(adjustl(buffer))

  case ('time')
     write(buffer,'(ES12.3)') time
     dirname = 'Data/time_'//trim(adjustl(buffer))

  case default
     call die('Unknown File Extension Type: '//trim(filext))
     print*, 'Choose iter or time'
  end select

  ! Open existing files
  open(unit = 10, status='old',action="read",file=trim(dirname)//'/'//trim(G_f),form='unformatted')
  open(unit = 20, status='old',action="read",file=trim(dirname)//'/'//trim(P_f),form='unformatted')
  open(unit = 30, status='old',action="read",file=trim(dirname)//'/'//trim(u_f),form='unformatted')
  open(unit = 40, status='old',action="read",file=trim(dirname)//'/'//trim(v_f),form='unformatted')

  ! Read existing files
  read(10) G (imin_:imax_  ,jmin_:jmax_  ,1:ndof); close(10)
  read(20) Pr(imin_:imax_  ,jmin_:jmax_  ,1:ndof); close(20)
  read(30) u (imin_:imax_+1,jmin_:jmax_  ,1:ndof); close(30)
  read(40) v (imin_:imax_  ,jmin_:jmax_+1,1:ndof); close(40)


end subroutine data_read_serial

!====================================!
!    Global Kinetic Energy Output    !
!====================================!
subroutine data_KE
  use data
  use variables
  use levelset
  implicit none

  ! Define Necessary Variables
  character(len=str_medium) :: filename
  character(len=str_medium) :: dirname
  character(len=str_medium), dimension(1,ndof+1) :: heading
  logical :: file_exists
  integer :: i,j

  ! Create the file name
  filename = 'GlobalKE.txt'

  ! Create the directory name
  dirname = 'Data'

  ! Create new directory
  ! call system('mkdir -p '// adjustl(trim(dirname)))

  ! Set file headings
  heading(1,1) = 'Time'
  heading(1,2) = 'gKE_0'
  do i = 3,ndof+1
     heading(1,i) = 'gKE_'//char(i-2)
  end do

  ! Open or create text file
  inquire(file=trim(dirname)//'/'//trim(filename),exist=file_exists)
  if (file_exists) then
     ! Replace an existing file
     open(unit = 10, status='replace',action="write",file=trim(dirname)//'/'//trim(filename))  
  else
     ! Open a new file
     open(unit = 10, status='new',action="write",file=trim(dirname)//'/'//trim(filename))  
  end if

  ! Write to text file (space delimited)
  write(10,*) (heading(1,j), j = 1,ndof+1)
  do i = 1,niter
     write(10,*) (gKE(i,j), j = 1,ndof+1)
  end do
  close(10)

  return
end subroutine data_KE

!=====================================!
!    Total Interfacial Area Output    !
!=====================================!
subroutine data_iArea
  use data
  use variables
  use levelset
  implicit none

  ! Define Necessary Variables
  character(len=str_medium) :: filename
  character(len=str_medium) :: dirname
  character(len=str_medium), dimension(1,ndof+1) :: heading
  logical :: file_exists
  integer :: i,j

  ! Create the file name
  filename = 'InterfacialArea.txt'

  ! Create the directory name
  dirname = 'Data'

  ! Set file headings
  heading(1,1) = 'Time'
  heading(1,2) = 'Area'

  ! Open or create text file
  inquire(file=trim(dirname)//'/'//trim(filename),exist=file_exists)
  if (file_exists) then
     ! Replace an existing file
     open(unit = 10, status='replace',action="write",file=trim(dirname)//'/'//trim(filename))  
  else
     ! Open a new file
     open(unit = 10, status='new',action="write",file=trim(dirname)//'/'//trim(filename))  
  end if

  ! Write to text file (space delimited)
  write(10,*) (heading(1,j), j = 1,2)
  do i = 1,niter
     write(10,*) (giVol(i,j), j = 1,2)
  end do
  close(10)

  return
end subroutine data_iArea

!================================================!
!     Calculate Mass and Volume Conservation     !
!================================================!
subroutine data_volume
  use data
  use variables
  use velocity
  use levelset
  implicit none

  integer :: i,j,kk,ll,tt
  real(WP) :: myGi2

  ! Calculate volume on a subgrid scale
  ! Calculate G at left, bottom, and corner cell face
  Gxi = 0.0_WP; Gyi = 0.0_WP; Gco = 0.0_WP
  Gxi(imin_:imax_+1,:,:) = 0.5_WP*(G(imin_:imax_+1,:,:)+G(imin_-1:imax_,:,:))
  Gyi(:,jmin_:jmax_+1,:) = 0.5_WP*(G(:,jmin_:jmax_+1,:)+G(:,jmin_-1:jmax_,:))
  Gco(imin_:imax_+1,jmin_:jmax_+1,:) = 0.25_WP*(G(imin_:imax_+1,jmin_:jmax_+1,:) + &
       G(imin_-1:imax_,jmin_:jmax_+1,:) + G(imin_:imax_+1,jmin_-1:jmax_,:) + &
       G(imin_-1:imax_,jmin_-1:jmax_,:))
  call comm_borders(Gxi); call comm_borders(Gyi); call comm_borders(Gco)

  ! Calculate new density
  rhok = 0.0_WP
  do tt = 1,ndof
     do ll = 1,ndof
        do kk = 1,ndof
           if (M(kk,ll,tt).gt.epsilon(0.0_WP)) then
              rhok(:,:,tt) = rhok(:,:,tt) + (rhoI(kk)*G(:,:,ll)-rhoE(kk)*G(:,:,ll))*M(kk,ll,tt)
           end if
        end do
     end do
     rhok(:,:,tt) = rhoE(tt) + rhok(:,:,tt)
  end do
  call comm_borders(rhok)

  ! Calculate density at left, bottom, and corner cell face
  Rco = 0.0_WP; rho_lef = 0.0_WP; rho_bot = 0.0_WP
  rho_lef(imin_:imax_+1,:,:) = 0.5_WP*(rhok(imin_:imax_+1,:,:)+rhok(imin_-1:imax_,:,:))
  rho_bot(:,jmin_:jmax_+1,:) = 0.5_WP*(rhok(:,jmin_:jmax_+1,:)+rhok(:,jmin_-1:jmax_,:))
  Rco(imin_:imax_+1,jmin_:jmax_+1,:) = 0.25_WP*(rhok(imin_:imax_+1,jmin_:jmax_+1,:) + &
       rhok(imin_-1:imax_,jmin_:jmax_+1,:) + rhok(imin_:imax_+1,jmin_-1:jmax_,:) + &
       rhok(imin_-1:imax_,jmin_-1:jmax_,:))
  call comm_borders(rho_lef); call comm_borders(rho_bot); call comm_borders(Rco)

  ! Find value of subgrid points (each cell divided into 4 subgrid cells)
  Gsum = 0.0_WP; iVol = 0.0_WP
  do i = imin_,imax_
     do j = jmin_,jmax_
        if ((0.25_WP*(G(i,j,1)+Gco(i,j,1)+Gxi(i,j,1)+Gyi(i,j,1))).gt.0.5_WP) then
           iVol(i,j,1) = iVol(i,j,1) + 0.25_WP
           Gsum(i,j,1) = Gsum(i,j,1) + 0.25_WP*dx*dy* &
                (0.25_WP*(rhok(i,j,1)+Rco(i,j,1)+rho_lef(i,j,1)+rho_bot(i,j,1)))
        end if
        if ((0.25_WP*(G(i,j,1)+Gco(i+1,j,1)+Gxi(i+1,j,1)+Gyi(i,j,1))).gt.0.5_WP) then
           iVol(i,j,1) = iVol(i,j,1) + 0.25_WP
           Gsum(i,j,1) = Gsum(i,j,1) + 0.25_WP*dx*dy* &
                (0.25_WP*(rhok(i,j,1)+Rco(i+1,j,1)+rho_lef(i+1,j,1)+rho_bot(i,j,1)))
        end if
        if ((0.25_WP*(G(i,j,1)+Gco(i,j+1,1)+Gxi(i,j,1)+Gyi(i,j+1,1))).gt.0.5_WP) then
           iVol(i,j,1) = iVol(i,j,1) + 0.25_WP
           Gsum(i,j,1) = Gsum(i,j,1) + 0.25_WP*dx*dy* &
                (0.25_WP*(rhok(i,j,1)+Rco(i,j+1,1)+rho_lef(i,j,1)+rho_bot(i,j+1,1)))
        end if
        if ((0.25_WP*(G(i,j,1)+Gco(i+1,j+1,1)+Gxi(i+1,j,1)+Gyi(i,j+1,1))).gt.0.5_WP) then
           iVol(i,j,1) = iVol(i,j,1) + 0.25_WP
           Gsum(i,j,1) = Gsum(i,j,1) + 0.25_WP*dx*dy* &
                (0.25_WP*(rhok(i,j,1)+Rco(i+1,j+1,1)+rho_lef(i+1,j,1)+rho_bot(i,j+1,1)))
        end if
     end do
  end do

  ! Communicate
  call comm_borders(iVol)
  call comm_borders(Gsum)

  ! Save interfacial mass
  myGi2 = sum(Gsum(imin_:imax_,jmin_:jmax_,1))
  call sum_real(myGi2,Gi2)
  if (iter.eq.0) Gi1 = Gi2
  AvgIntDiff = AvgIntDiff + 100_WP*(Gi2-Gi1)/Gi1

  ! Save interfacial area
  myGi2 = sum(iVol(imin_:imax_,jmin_:jmax_,1))
  call sum_real(myGi2,giVol(iter,2))
  giVol(iter,1) = time

  return
end subroutine data_volume

!=========================================!
!          Load Monte Carlo Data          !
!=========================================!
subroutine data_mc
  use data
  use variables
  implicit none

  ! Read text file with Monte Carlo inputs


end subroutine data_mc

!=====================================================================!
!          Compile Monte Carlo Data and Calculate Statistics          !
!=====================================================================!
subroutine data_mc_stats
  use data
  use variables
  implicit none

  ! Compute statistics on important variables


end subroutine data_mc_stats


