! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module vof
  use grid
  use variables
  use basis
  use polynomial
  use communication
  use velocity
  implicit none

end module vof

!==============================!
!        Initialization        !
!==============================!
subroutine vof_init
  use variables
  use levelset
  use math
  use io
  implicit none

  ! Calculate initial surface tension
  call levelset_curvature

  ! Find initial probabilities
  call levelset_probability

  ! Save interfacial fluid mass (for conservation tracking)
  AvgIntDiff = 0.0_WP
  call data_volume
  Gsave = G

  return
end subroutine vof_init

!=================================!
!    Level Set RHS - FLUX-HOUC    !
!=================================!
subroutine vof_rhs
  use vof
  use velocity
  implicit none

  integer :: i,j,tt,k,l

  ! Zero arrays
  rhsG=0.0_WP; Gupx=0.0_WP; Gupy=0.0_WP

  ! Calculate Upwinded Level Set
  ! Interpolates level set values to the cell edge
  do j=jmin_-1,jmax_+1
     do i=imin_-1,imax_+1
        do k=1,ndof
           ! G at left cell face
           if (u(i,j,k).ge.0.0_WP) then
              Gupx(i,j,k)=sum(houc_xp(:,1)*G(i-ceiling(real(nhouc,WP)/2.0_WP):i+(floor(real(nhouc,WP)/2.0_WP)-1),j,k))
           else
              Gupx(i,j,k)=sum(houc_xm(:,1)*G(i-floor(real(nhouc,WP)/2.0_WP):i+floor(real(nhouc,WP)/2.0_WP),j,k))
           end if
           ! G at bottom cell face
           if (v(i,j,k).ge.0.0_WP) then
              Gupy(i,j,k)=sum(houc_yp(:,1)*G(i,j-ceiling(real(nhouc,WP)/2.0_WP):j+(floor(real(nhouc,WP)/2.0_WP)-1),k))
           else
              Gupy(i,j,k)=sum(houc_ym(:,1)*G(i,j-floor(real(nhouc,WP)/2.0_WP):j+floor(real(nhouc,WP)/2.0_WP),k))
           end if
        end do
     end do
  end do

  ! Communication
  call comm_borders(Gupx)
  call comm_borders(Gupy)

  ! Calculate needed information
  Gdx = 0.0_WP; Gdy = 0.0_WP
  Gdx(imin_:imax_,:,:) = (Gupx(imin_+1:imax_+1,:,:)-Gupx(imin_:imax_,:,:))*dxi
  Gdy(:,jmin_:jmax_,:) = (Gupy(:,jmin_+1:jmax_+1,:)-Gupy(:,jmin_:jmax_,:))*dyi
  call comm_borders(Gdx); call comm_borders(Gdy)
  Uint = 0.0_WP; Vint = 0.0_WP
  Uint(imin_:imax_,:,:) = 0.5_WP*(u(imin_:imax_,:,:) + u(imin_+1:imax_+1,:,:))
  Vint(:,jmin_:jmax_,:) = 0.5_WP*(v(:,jmin_:jmax_,:) + v(:,jmin_+1:jmax_+1,:))
  call comm_borders(Uint); call comm_borders(Vint)

  ! Compute RHS by looping over Mklt
  ! Relocates level set values to cell center
  do tt=1,ndof
     do k=1,ndof
        do l=1,ndof
           if (M(k,l,tt).gt.0.0_WP) then
              ! Add terms to RHS
              rhsG(:,:,tt) = rhsG(:,:,tt) - (Uint(:,:,k)*Gdx(:,:,l)+Vint(:,:,k)*Gdy(:,:,l))*M(k,l,tt)
           end if
        end do
     end do
  end do

  ! Communication
  call comm_borders(rhsG)

  return
end subroutine vof_rhs

!====================================================!
!       Piecewise Linear Interface Calculation       !
!====================================================!
subroutine vof_normal
  use vof
  use math
  implicit none
  integer :: i,j,k
  integer :: delt
  real(WP) :: topx,botx,topy,boty

  ! Zero out arrays
  Nox = 0.0_WP; Noy = 0.0_WP

  ! Calculate Normals with a 2-D Least Squares Approach
  ! Calculated at the cell center (where G is located)
  ! Only works with a structured, non-variable grid
  delt = floor(real(gridbase,WP)/2.0_WP)
  do i = imin_-1,imax_+1
     do j = jmin_-1,jmax_+1
        do k = 1,ndof
           ! Calculate Averages
           ! Central point xm(i) and ym(j) are average (0)
           ! Calculate parts of fraction
           ! dG/dx
           topx = sum(matmul((xm(i-delt:i+delt)-xm(i)),G(i-delt:i+delt,j-delt:j+delt,k)))
           botx = real(gridbase,WP)*sum((xm(i-delt:i+delt)-xm(i))**2)
           ! dG/dy
           topy = sum(matmul((ym(j-delt:j+delt)-ym(j)),transpose(G(i-delt:i+delt,j-delt:j+delt,k))))
           boty = real(gridbase,WP)*sum((ym(j-delt:j+delt)-ym(j))**2)
           ! Calculate Normals
           Nox(i,j,k) = -topx/botx
           Noy(i,j,k) = -topy/boty
        end do
     end do
  end do

  ! Communication
  call comm_borders(Nox)
  call comm_borders(Noy)

  return
end subroutine vof_normal

! Density calculation is the same as with level set










