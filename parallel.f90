! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!========================================!
!          Parallel environment          !
!========================================!
module parallel
  use variables
  implicit none
  include 'mpif.h'

  integer :: rank, rankx, ranky, nproc
  integer :: comm, comm_x, comm_y
  integer, dimension(4) :: request_send
  integer, dimension(4) :: request_recv
  integer :: ierr, tag, tag2, comm_2D, status(mpi_status_size)

  integer, parameter :: root=0

end module parallel

!==============================!
!          Initialize          !
!==============================!
subroutine parallel_init
  use parallel
  implicit none

  ! Initialize parallel environment
  call mpi_init(ierr)
  ! Duplicate communicator
  call mpi_comm_dup(mpi_comm_world,comm,ierr)

  ! Get size and rank
  call mpi_comm_size(comm, nproc, ierr)
  call mpi_comm_rank(comm, rank, ierr)

  call parallel_cartesian

end subroutine parallel_init

!============================!
!          Finalize          !
!============================!
subroutine parallel_final
  use parallel
  implicit none

  ! Finalize parallel environment
  call mpi_finalize(ierr)

end subroutine parallel_final

!================================================!
!          Cartesian processor geometry          !
!================================================!
subroutine parallel_cartesian
  use variables
  use parallel
  use io
  implicit none
  integer, dimension(2) :: dims
  logical, dimension(2) :: peri
  integer, dimension(2) :: coords

  dims(1)=px
  dims(2)=py

  peri=.false.
  if (isxper) peri(1)=.true.
  if (isyper) peri(2)=.true.

  ! Check for correct number of procs 
  if (px*py.ne.nproc) then
     call die("Wrong number of processors")
  end if

  ! Create caratesian grid of processors
  call mpi_cart_create(comm,2,dims,peri,1,comm_2D,ierr)
  call mpi_cart_coords(comm_2D,rank,2,coords,ierr)
  rankx=coords(1)
  ranky=coords(2)

  ! Find neighboring processors for communication
  call mpi_cart_shift(comm_2D,0,-1,Lsend,Lrecv,ierr)
  call mpi_cart_shift(comm_2D,0, 1,Rsend,Rrecv,ierr)
  call mpi_cart_shift(comm_2D,1,-1,Bsend,Brecv,ierr)
  call mpi_cart_shift(comm_2D,1, 1,Tsend,Trecv,ierr)

end subroutine parallel_cartesian
