! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

program main
  use variables
  implicit none
  integer :: i

  ! Initialize the program
  call initialize

  ! Check for Monte Carlo simulation
  if (.not.MCS) then
     ! Run core program
     call core
  else
     ! Run Monte Carlo simulations
     do i = 1,mci
        call data_mc
        call core
        call data_mc_stats
     end do
  end if

  ! Finalize simulation and end program
  call finalize

end program main

!=========================!
!        Initialize       !
!=========================!
subroutine initialize
  use variables
  use velocity
  use pressure
  use levelset
  use parallel
  use fourier
  use basis
  use grid
  use io
  implicit none

  integer :: kk, verr

  ! Initialize time
  time=0.0_WP

  ! Initialize iteration counter
  iter=0

  ! Read inputs file 
  call io_init

  ! Initialize core variables
  call io_read_input

  ! Setup parallel environment
  call parallel_init

  ! Setup grid
  call grid_create

  ! Start timer for initialization
  call timing_start(4)

  ! Initialize basis
  call timing_start(5)
  call basis_init
  call timing_end(5)

  ! Initialize HOUC coefficients
  call houc_init

  ! Initialize Quadrature
  call quadrature_init

  ! Create data folder
  call system('mkdir -p '// adjustl(trim('Data')))

  ! Initialize velocity update
  call velocity_init

  ! Initialize uncertainty quantification
  call uq_init

  ! Initialize pressure solver
  call pressure_init

  ! Initialize level set
  if (use_multiphase) then
     call levelset_init
     call MPI_BARRIER(COMM, verr)
  else
     call density_init
  end if

  ! Initialize simulation
  call simulation_init

  ! Initialize source term
  call velocity_source_init

  ! Start from a saved point
  if (restart) then

     ! Import data
     !call data_read_serial
     call data_silo_read

     ! Communication of incoming data
     call comm_borders(G); call comm_borders(Pr)
     call comm_borders(u); call comm_borders(v)

     ! Enforce boundary conditions
     call velocity_bc; call geometry_bc
     do kk=1,nghost
        if (rankx.eq.0   ) Pr(imin_-kk,:,:) = Pr(imin_,:,:)
        if (rankx.eq.px-1) Pr(imax_+kk,:,:) = Pr(imax_,:,:)
        if (ranky.eq.0   ) Pr(:,jmin_-kk,:) = Pr(:,jmin_,:)
        if (ranky.eq.py-1) Pr(:,jmax_+kk,:) = Pr(:,jmax_,:)
     end do

     ! Calculate curvature and density
     call levelset_curvature
  end if
  call MPI_BARRIER(COMM, verr)

  ! Initialize visit 
  call visit_init

  ! Output first data set
  call visit_data

  ! Initialize visit_restart
  call visit_restart_init
  call visit_restart_data


  ! Finish timing initialization
  call timing_end(4)

  ! Write simulation summary
  if (rank.eq.root) then
     print*, '================================================================================================================='
     print*, '===                                                   multiUQ                                                 ==='
     print*, '===                                            Simulation Parameters                                          ==='
     print*, '================================================================================================================='
     print*, '  Velocity Simulation =                   ',trim(simu)
     print*, '  Spatial and Mesh Information'
     print*, '      X Domain =             [',real(xmin,SP),real(xmax,SP),']'
     print*, '      Y Domain =             [',real(ymin,SP),real(ymax,SP),']'
     print*, '      Grid points =          ',nx,'X',ny,'Y'
     print*, '      Ghost Cells =               ',nghost
     print*, '  Time Information'
     print*, '      CFL =                           ',real(CFL,SP)
     print*, '      Total Iterations =          ',niter
     print*, '  Fluid Properties'
     print*, '      Interfacial Density =           ',real(rho_io,SP)
     print*, '      Density Ratio =                 ',real(max(rho_io/rho_ioE,rho_ioE/rho_io))
     if (use_multiphase) then
        print*, '  Level Set Information'
        print*, '      Level Set Geometry =                  ',trim(Gshape)
        print*, '      High Order Upwind Central =  ',nhouc
        print*, '      Normal Least Squares Base =  ',gridbase
        print*, '      Initial Convergence Steps =  ',ilsc
        print*, '      Reinitialization CFL =          ',real(reCFL,SP)
        print*, '      F-value =                       ',real(F_tau,SP)
        print*, '      Reinitialization Steps =     ',mrs
     end if
     if (is_uq) then
        print*, '  Uncertainty Information'
        print*, '      Degrees of Freedom =         ',ndof
        print*, '      Nu dof =                     ',ndof_nu
        print*, '      Starting Variation =            ',real(sigmaI,SP)
     end if
     print*, '=================================================================================================================', &
          '====='
  end if

  ! Intialize std-out
  if (rank.eq.root) then
     ! write (*,'(A)') '   Iter     Time           max(U)         max(V)       P residual   Pstep     Div(U)        ReinSteps   CNsteps'
     print*,'  Iter      Time           max(U)         max(V)       Presidual    Pstep      Div(U)         dInt%       CNI    PEI'
  end if


end subroutine initialize

!=========================!
!       Core Program      !
!=========================!
subroutine core
  use simulation
  use variables
  use grid
  implicit none

  integer :: verr

  ! Start timers
  call timing_start(1)
  call timing_pause(1)
  call timing_start(2)
  call timing_pause(2)
  call timing_start(3)

  ! Write initial condition
  call write_status

  do while (.not.isdone)

     ! Update counter
     iter = iter + 1

     ! Update solution
     call simulation_step

     ! Update time
     time = time + dt

     ! Write iteration to std-out
     call write_status

     ! Save solution to a file
     call MPI_BARRIER(COMM, verr)
     call visit_data

     ! Output a data file
     if ((mod(iter,wrtfr).eq.0)) then
        if (rank.eq.root) then
           call data_KE
           call data_iArea
        end if
        call data_write_serial
     end if

     ! Output restart file
     call visit_restart_data

     ! Check if done
     call check_done

  end do

  ! Finish Timing
  call timing_end(3)
  call timing_resume(1)
  call timing_end(1)
  call timing_resume(2)
  call timing_end(2)
  call timing_output

end subroutine core

!=========================!
!         Finalize        !
!=========================!
subroutine finalize
  use variables
  use parallel
  use fourier
  implicit none

  ! Output Global Kinetic Energy to file
  if (rank.eq.root) call data_KE

  ! Output total interfacial area to file
  if (rank.eq.root) call data_iArea

  ! Save final solution to a file
  call data_write_serial

  ! Close FFTW
  if (use_FFTW) call fourier_final

  ! Close HYPRE
  if (use_HYPRE) call hypre_final

  ! Close parallel environment
  call parallel_final

end subroutine finalize


!=========================!
!      Check if done      !
!=========================!
subroutine check_done
  use variables
  use parallel
  use levelset
  use math
  implicit none

  if ((iter.gt.(niter-1)).or.(time.gt.simulation_time)) then

     ! Kill simulation
     isdone=.true. 

     ! Output closing information
     if (rank.eq.root) then
        print*,  '============================================================================================================'
        print*,  '===                                           Simulation Complete!                                       ==='
        print*,  '============================================================================================================'
        print *, '   Time step                         ',dt
        print *, '   Psuedo-time step                  ',dtre
        print *, '   Integral Information              '
        print *, '      Original Integral:             ',Gi1
        print *, '      Ending Integral:               ',Gi2
        print *, '      Integral Percent Difference:   ',100_WP*(Gi2-Gi1)/Gi1
        print *, '      Avg Integral Percent Diff:     ',AvgIntDiff/real(iter,WP)
        print *, '   Reinitialization Convergence:     ',Gconverge
        print *, '   Average Maximum Divergence:       ',div_avg/real(iter,WP)
        print *, '   Total Pressure Iterations:        ',pressure_iterations
     end if

  end if

  return
end subroutine check_done

!=========================!
! Write status to std-out !
!=========================!
subroutine write_status
  use variables
  use parallel
  use levelset
  implicit none

  real(WP) :: umax_local, umax
  real(WP) :: vmax_local, vmax

  ! Calculate max velocities
  umax_local = maxval(abs(u(:,:,1)))
  call max_real(umax_local,umax)
  vmax_local = maxval(abs(v(:,:,1)))
  call max_real(vmax_local,vmax)

  if (rank.ne.root) return

  write (*,'(I6,4ES15.5,I7,1ES15.5,1ES15.5,I6,I8)') iter, time, &
       umax, vmax, p_res, &
       pressure_steps, vel_divergence, 100_WP*(Gi2-Gi1)/Gi1, ctr1, ctr3

  return
end subroutine write_status


!=========================!
!        Call die         !
!=========================!
subroutine die(text)
  use parallel
  implicit none

  character(len=*), intent(in) :: text
  print *, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
  print *, ' ', trim(text)
  print *, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
  call mpi_abort(MPI_COMM_WORLD,ierr)

end subroutine die
