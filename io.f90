! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module io
  use variables
  implicit none

  integer :: nline
  character(len=100), dimension(:), allocatable :: labels
  character(len=200), dimension(:), allocatable :: values

  ! Define interface for reading inputs
  interface read_input
     module procedure read_input_int
     module procedure read_input_real
     module procedure read_input_char
     module procedure read_input_chararray
     module procedure read_input_logical
  end interface read_input

contains

  !====================!
  ! Read integer input !
  !====================!
  subroutine read_input_int(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    integer, intent(out) :: myvalue
    integer, optional, intent(in) :: default
    integer :: n,ios

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from inputs file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in inputs file')
    end if

    return 
  end subroutine read_input_int

  !===================!
  !  Read real input  !
  !===================!
  subroutine read_input_real(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    real(WP), intent(out) :: myvalue
    real(WP), optional, intent(in) :: default
    integer :: n,ios

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from inputs file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in inputs file')
    end if

    return 
  end subroutine read_input_real

  !======================!
  ! Read character input !
  !======================!
  subroutine read_input_char(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    character(len=*), intent(out) :: myvalue
    character(len=*), optional, intent(in) :: default
    integer :: n,ios

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),*,iostat=ios) myvalue
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from inputs file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in inputs file')
    end if

    return 
  end subroutine read_input_char

  !============================!
  ! Read character array input !
  !============================!
  subroutine read_input_chararray(mylabel,myvalue)
    implicit none
    character(len=*), intent(in) :: mylabel
    character(len=*), dimension(:), intent(out) :: myvalue
    character(len=200) :: buffer
    integer :: n,nn,ios,count,ibeg

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),'(A)',iostat=ios) buffer
          if (ios == 0 ) then
             ! Parse buffer to find entries
             count=1 ! Number of entry in array
             ibeg=1  ! Index of beginning of entry
             do nn=1,len_trim(buffer)+1
                if (buffer(nn:nn).eq.' '.or.nn.eq.len_trim(buffer)+1) then ! Reached end of entry
                   if (len_trim(buffer(ibeg:nn-1)).gt.0) then
                      myvalue(count)=buffer(ibeg:nn-1) ! Save entry
                      count=count+1  ! Update count for next entry
                   end if
                   ibeg=nn+1  ! Update beginning of entry for next entry
                end if
             end do
             return
          else
             call die('Problem reading '//trim(mylabel)//' from inputs file')
          end if
       end if
    end do

    ! Didn't find matching label
    call die('Missing entry "'//trim(mylabel)//'" in inputs file')

    return 
  end subroutine read_input_chararray

  !====================!
  ! Read logical input !
  !====================!
  subroutine read_input_logical(mylabel,myvalue,default)
    implicit none
    character(len=*), intent(in) :: mylabel
    logical, intent(out) :: myvalue
    logical, optional, intent(in) :: default
    integer :: n,ios,conv

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          if (len_trim(adjustl(values(n))).eq.1) then ! 0 or 1
             read(values(n),*,iostat=ios) conv
             if (conv.eq.1) then
                myvalue=.true.
             else
                myvalue=.false.
             end if
          else
             read(values(n),*,iostat=ios) myvalue
          end if
          if (ios == 0 ) then
             return
          else
             call die('Problem reading '//trim(mylabel)//' from inputs file')
          end if
       end if
    end do

    ! Didn't find matching label
    if (present(default)) then ! use default value
       myvalue=default
    else ! End simulation
       call die('Missing entry "'//trim(mylabel)//'" in inputs file')
    end if

    return 
  end subroutine read_input_logical

  !============================!
  ! Read character array input !
  !============================!
  subroutine count_input(mylabel,count)
    implicit none
    character(len=*), intent(in) :: mylabel
    integer, intent(out) :: count
    character(len=200) :: buffer
    integer :: n,nn,ios,ibeg

    ! Loop over input and find the requested input
    do n=1,nline
       if (trim(mylabel).eq.trim(labels(n))) then
          read(values(n),'(A)',iostat=ios) buffer
          if (ios == 0 ) then
             ! Parse buffer to find entries
             count=0 ! Zero counter
             ibeg=1  ! Index of beginning of entry
             do nn=1,len_trim(buffer)+1
                if (buffer(nn:nn).eq.' '.or.nn.eq.len_trim(buffer)+1) then ! Reached end of entry
                   if (len_trim(buffer(ibeg:nn-1)).gt.0) then
                      count=count+1  ! Update count
                   end if
                   ibeg=nn+1  ! Update beginning of entry for next entry
                end if
             end do
             return
          else
             call die('Problem reading '//trim(mylabel)//' from inputs file')
          end if
       end if
    end do

    ! Didn't find matching label
    call die('Missing entry "'//trim(mylabel)//'" in inputs file')

    return 
  end subroutine count_input

end module io

!=============================!
! Read input file into arrays !
!=============================!
subroutine  io_init
  use io
  implicit none

  ! Input related variables
  character(len=300) :: buffer
  character(len=100), dimension(:), allocatable :: tmp_labels
  character(len=200), dimension(:), allocatable :: tmp_values
  integer :: array_size
  integer, parameter :: nadded = 20
  integer :: pos
  integer, parameter :: fh = 15
  integer :: ios = 0

  ! Allocate arrays (start with 100 lines)
  array_size=100
  allocate(labels(100))
  allocate(values(100))

  ! Open file
  open(unit=fh, file="inputs")

  ! Initialize line counter
  nline=0

  ! Loop over lines in file
  do while (ios == 0)
     ! ios is negative if an end of record condition is encountered or if
     ! an endfile condition was detected.  It is positive if an error was
     ! detected.  ios is zero otherwise.

     ! Read line into buffer
     read(fh, '(A)', iostat=ios) buffer

     ! Check end of file
     if (ios /= 0) exit

     ! Check for comment
     pos = scan(buffer, '!%#')
     if (pos.gt.0) cycle

     ! Split buffer into label and values at colon
     pos = scan(buffer, ':')
     if (pos.gt.0) then ! valid line (contains colon)
        nline = nline+1    
        labels(nline) = buffer(1:pos-1)
        values(nline) = buffer(pos+1:)
     end if

     ! Reallocate arrays if needed
     if (nline.eq.array_size) then
        ! Store arrays in tmp
        allocate(tmp_labels(array_size)); tmp_labels=labels
        allocate(tmp_values(array_size)); tmp_values=values
        ! Reallocate arrays
        deallocate(labels); allocate(labels(array_size+nadded))
        deallocate(values); allocate(values(array_size+nadded))
        ! Transfer values from tmp to arrays
        labels(1:array_size)=tmp_labels; deallocate(tmp_labels)
        values(1:array_size)=tmp_values; deallocate(tmp_values)
        ! Update array size
        array_size=array_size+nadded
     end if

  end do
  close(fh)

  return
end subroutine io_init


!===================================!
!          Read input file          !
!===================================!
subroutine io_read_input
  use io
  use variables
  implicit none

  ! Fluid parameters
  call read_input('Interfacial Fluid Density',rho_io)
  call read_input('Interfacial Density Variation',sigmaDI)
  call read_input('Extrafacial Fluid Density',rho_ioE)
  call read_input('Extrafacial Density Variation',sigmaDE)
  call read_input('Interfacial Kinematic Viscosity',nu_io)
  call read_input('Interfacial Viscosity Variation',sigmaVI)
  call read_input('Extrafacial Kinematic Viscosity',nu_ioE)
  call read_input('Extrafacial Viscosity Variation',sigmaVE)
  call read_input('Surface Tension Coefficient',gamma)
  call read_input('Surface Tension Variation',sigmaG)

  ! Various physical parameters
  call read_input('Gravity?',gravity)
  call read_input('Gravitational Acceleration',grav_acc)
  call read_input('Gravitational Variation',grav_var)

  ! Simulation Inputs
  call read_input('niter',niter)
  call read_input('Velocity Simulation',simu)
  call read_input('Write Frequency',wrtfr)
  call read_input('File Type',filext)

  ! Restarting
  call read_input('Restart',restart)

  ! Time Inputs
  call read_input('CFL',CFL)
  call read_input('Reinitialization CFL',ReCFL)
  call read_input('Minimum Time Step',dtmin)
  call read_input('Psuedo-time F-value',F_tau)
  call read_input('Time cut off',simulation_time)

  ! Pressure inputs
  call read_input('Pressure-max-steps',pressure_max_steps)
  call read_input('Pressure-cvg',cvg_criteria)
  call read_input('Use Gauss-Seidel solver?',testPr)
  call read_input('Use multigrid solver?',use_HYPRE)
  call read_input('Use FFT?',use_FFTW)
  call read_input('Pressure estimation iterations',pei)

  ! Boundary Conditions
  call read_input('No Flux?',flux)
  call read_input('xper',isxper) 
  call read_input('yper',isyper)

  ! Parallelism
  call read_input('Processors in x',px)
  call read_input('Processors in y',py)

  ! Uncertainty
  call read_input('Uncertainty Quantification',is_uq)
  call read_input('Basis order',order,1)
  call read_input('Nu basis order',order_nu,1)
  call read_input('Uncertain Variables',ndims,1)
  call read_input('Starting Variation',sigmaI)
  call read_input('Monte Carlo Sampling?',MCS)
  call read_input('Monte Carlo Iterations',mci)

  ! Import level set variables
  call read_input('Use multiphase?',use_multiphase)
  call read_input('Higher Order Upwind Central',nhouc)
  call read_input('Crank-Nicholson Iterations',cni)
  call read_input('Reinitialization Steps',mrs)
  call read_input('Normal Grid Base',gridbase)
  call read_input('Initial Level Set Convergence Steps',ilsc)
  call read_input('Level set shape',Gshape)
  call read_input('Output LS Probability?',LSProbCalc)
  call read_input('Level Set Test?',LST)

  ! Geometry Inputs
  call read_input('xo',xo)
  call read_input('yo',yo)
  call read_input('Circle D',D)
  call read_input('a',semimajor)
  call read_input('b',semiminor)

  ! Velocity inputs
  call read_input('Incoming U magnitude', Umag)
  call read_input('Incoming V magnitude', Vmag)
  call read_input('Incoming U variation', Umag_var)
  call read_input('Incoming V variation', Vmag_var)

  ! Quadrature input
  call read_input('Quad Points',quadpts)

  ! Check HUOC & LS schemes have odd base
  if (mod(gridbase,2).eq.0) gridbase = gridbase + 1
  if (mod(nhouc,2).eq.0) nhouc = nhouc + 1

  ! Calculate the number of ghost cells
  nghost = max(floor(real(gridbase,WP)/2.0_WP),max(2,ceiling(real(nhouc,WP)/2.0_WP)))+1

  return
end subroutine io_read_input



